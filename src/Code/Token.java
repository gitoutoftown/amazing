package Code;

public class Token 
{
	int _value;
	
	/**
	 * Token(int v) is the non-default constructor for Token class which sets the integer value for the Token
	 * 	  
	 * @param value
	 */
	public Token(int value)
	{
		if((value <= 20 && value>= 0) || value == 25)
		{
			_value = value;
		}
	}
	
	/**
	 * Default constructor, doesn't do anything 
	 */
	public Token()
	{
		
	}
	
	/**
	 * getValue() returns the integer value stored in the classes instance variable 
	 * 
	 * @return integer value of a token 
	 */
	public int getValue()
	{
		return _value;
	}
	
	/**
	 * setValue(int v) has an integer value as a parameter, if the value is between 1-20 and 25 will return true
	 * else it will return false
	 * 
	 * @param value integer value to be set as the tokens value
	 * @return boolean value if the integer value was properly or improperly set for the token
	 */
	public boolean setValue(int value)
	{
		if((value <= 20 && value>= 0) || value == 25)
		{
			_value = value;
			return true;
		}
		return false;
	}

}
