package Code;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.JComponent;

public class PawnMoveListener implements MouseListener {
	
	private GUI _gui;
	
	private Tile _t1, _t2;
	private Pawn _p;
	
	/**
	 * PawnMoveListener(GUI gui, Pawn p) has parameters GUI and pawn, initializes the instance variables 
	 * 
	 * @param gui the current setup of the GUI
	 * @param p the pawn of the player that is moving 
	 */
	public PawnMoveListener(GUI gui, Pawn p){
		_gui = gui;
		_p = p;
	}

	/**
	 * mouseClicked(MouseEvent e): When the Tile is clicked on by the user it calls method, movePawn, which moves the pawn
	 *                             and updates the GUI
	 */
	@Override
	public void mouseClicked(MouseEvent e) {
		// TODO Auto-generated method stub
		Tile t = (Tile) e.getSource();
		if (_p.movePawn(t))
		{
			_gui.updateGameWindow();
			_gui.breakDownGB(this);
			_gui.setUpTokenPickUp(t);
		}
		
	}

	@Override
	public void mousePressed(MouseEvent e) {
		// TODO Auto-generated method stub
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		// TODO Auto-generated method stub
	}

	@Override
	public void mouseEntered(MouseEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseExited(MouseEvent e) {
		// TODO Auto-generated method stub

	}

}
