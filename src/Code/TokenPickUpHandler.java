package Code;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class TokenPickUpHandler implements ActionListener {
	
	private GUI _gui;
	
	/**
	 * You should know what this does
	 * @param gui, GUI!!!!!!!
	 */
	public TokenPickUpHandler(GUI gui){
		_gui = gui;
	}

	/**
	 * calls pickUpToken method on the GUI, removing the token from the game board 
	 */
	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		_gui.pickUpToken();	
	}

}
