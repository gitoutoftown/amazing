package Code;

import java.util.HashSet;

public class Path{
	//ensures that two of the same tiles are not inserted
	private HashSet<Tile> _tileList;
	
	/**
	 * default constructor, creates a instantiates the HashSet()
	 */
	public Path(){
		_tileList = new HashSet<Tile>();
	}
	
	/**
	 * add adds a tile to the list, returns true if added, false otherwise
	 */
	public boolean add(Tile t){
		return _tileList.add(t);
	}
	
	/**
	 * contains() searches the HashSet for a tile
	 * 
	 * @param t Tile being searched for in the HashSet
	 * @return returns true if the list contains the tile false otherwise
	 */
	public boolean contains(Tile t){
		return _tileList.contains(t);
	}
	
	/**
	 * returnList() returns the HashSet of tiles
	 * 
	 * @return the entire list of the pawns possible tile moves
	 */
	public HashSet<Tile> returnList(){
		return _tileList;
	}
	
	/**
	 * remove() removes a tile from the _tileList and returns true if removed false otherwise
	 * 
	 * @param t	Tile to be removed
	 * @return returns true if the tile was removed
	 */
	public boolean remove(Tile t){
		return _tileList.remove(t);
	}
	 
}
