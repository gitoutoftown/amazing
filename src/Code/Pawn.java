package Code;

import java.util.ArrayList;

import Code.GameBoard;
import Code.Path;
import Code.Tile;

public class Pawn 
{
	private Tile _CurrentTile;
	@SuppressWarnings("unused")
	private java.awt.Color _CurrentColor;
	private Path _path;
	private GameBoard _gb;
	private String _playerName = "";
	private ArrayList<Token> _pawnTokens = new ArrayList<Token>();
	
	public Pawn ()
	{
	}
	/**
	 * Method Pawn sets the instance variables
	 * 
	 * @param currenttile Is the current tile of the player
	 * @param currentColor Is the color of the player's pawn during game play
	 * @param gb IS the game board the players are playing on
	 */
	public Pawn(Tile currenttile, java.awt.Color currentcolor, GameBoard gb)
	{
		_CurrentTile = currenttile;
		_CurrentColor = currentcolor;
		_gb = gb;
	}
	//get the Path for this player
	/**
	 * findPath() method finds the possible tiles the player can move after calling generatePath()
	 * on the game board
	 */
	public void findPath()
	{
		_path=_gb.generatePath(_CurrentTile);
	}
	
	/**
	 * movePawn() if a pawn was shifted of the edge of the board and into the free tile,
	 * then the pawn is shifted to the start of row or column that was shifted
	 * 
	 * @param t tile the pawn is on
	 * @return returns true if pawn was moved form tile
	 */
	public boolean movePawn(Tile t)
	{
		findPath();
		if(_path.contains(t))
			{
			_CurrentTile.removePawn(this);
			_CurrentTile = t;
			_CurrentTile.setPawn(this);
			
			return true;
			}
		else return false;
		
	}
	
	/**
	 * sets the players name
	 * 
	 * @param s string of the players name
	 */
	public void setPlayerName (String s)
	{
		_playerName = s;
	}
	
	/**
	 * returns the string representation of the players name
	 * 
	 * @return string of players name
	 */
	public String getPlayerName()
	{
		return _playerName;
	}
	
	/**
	 * returns the tile the player is currently on
	 * 
	 * @return tile the player is on
	 */
	public Tile getCurrentTile(){
		return _CurrentTile;
	}
	
	/**
	 * returns the token the player has picked up from the tile
	 * 
	 * @return the token
	 */
	public Token pickToken()
	{
			_pawnTokens.add(_CurrentTile.getToken());
			Token t = _CurrentTile.getToken();
			_CurrentTile.setToken(null);
			_gb.increaseTokenByOne();
			return t;
	}
}
