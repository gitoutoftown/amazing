package Code;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JButton;

public class TurnHandler implements ActionListener {
	
	//private JButton _p1, _p2, _p3, _p4;
	public ArrayList<JButton> _list;
	public GUI _gui;
	

	/**
	 * adds all the players to an array list 
	 * 
	 * @param p1 pawn one
	 * @param p2 pawn two
	 * @param p3 pawn three
	 * @param p4 pawn four
	 * @param gui current GUI
	 */
	public TurnHandler(JButton p1, JButton p2, JButton p3, JButton p4, GUI gui){
		_list = new ArrayList<>();
		_list.add(p1);
		_list.add(p2);
		_list.add(p3);
		_list.add(p4);
		_gui = gui;
	}

	/**
	 * When a user clicks on their button it changes the text of it indicating them to move, 
	 * then picks the next player
	 * 
	 */
	@Override
	public void actionPerformed(ActionEvent e) {
		for (int i = 0; i<4; i++){
			if (e.getSource() == _list.get(i)){
				_gui.setUpMouseListeners(_gui);
				
				_list.get(i).setEnabled(false);
				_list.get(i).setText("SHIFT!");
			}
		}
	}

}
