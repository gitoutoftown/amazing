package Code;

import java.awt.FlowLayout;
import java.awt.Image;
import java.util.ArrayList;
import java.util.HashSet;

import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;

@SuppressWarnings({ "unused", "serial" })
public class Tile extends JLabel
{
	
	private boolean _north,_east, _south,_west;
	private boolean _hasToken = false;
	private HashSet<Pawn> _list;
	private Pawn _pawn;
	private int _row, _column;
	private Token _token = null;
	private int _playercount = 0;
	/**
	 * Default constructor, sets the instance variables, North, East, South, and West, and sets the coordinates 
	 * of the Tile
	 * 
	 * @param north The top of the Tile block, is either true or false 
	 * @param east The right side of the Tile Block, is either true or false
	 * @param south  The bottom of the Tile block, is set to either true or false
	 * @param west The left of the Tile block, is set to either true or false
	 * @param r The x-coordinate of the Tile in the game board 
	 * @param c The y-coordinate of the Tile in the game board
	 */
	public Tile(boolean north, boolean east, boolean south, boolean west, int r, int c)
	{
		_list = new HashSet<>();
		_pawn = null;
		_north = north;
		_east = east;
		_south = south;
		_west = west;
		_column=c;
		_row=r;
		_token = null;
		
		//JLabel imgLabel = new JLabel(new ImageIcon("labyrinth1.jpg"));
		//add(imgLabel);
	}
	
	/**
	 * getNorth() is a boolean method that returns the boolean value of the north of the Tile
	 * 
	 * @return the boolean value of the north of the tile
	 */
	public boolean getNorth()
	{
		return _north;
	}
	
	/**
	 * getEast() is a boolean method that returns the boolean value of the east of the Tile
	 * 
	 * @return the boolean value of the east of the tile
	 */
	public boolean getEast()
	{
		return _east;
	}
	
	/**
	 * getSouth() is a boolean method that returns the boolean value of the south of the Tile
	 * 
	 * @return the boolean value of the south of the tile
	 */
	public boolean getSouth()
	{
		return _south;
	}
	
	/**
	 * getWest() is a boolean method that returns the boolean value of the north of the Tile
	 * 
	 * @return the boolean value of the west of the tile
	 */
	public boolean getWest()
	{
		return _west;
	}
	
	/**
	 * SetPawn() assigns the _pawn instance variable for the class
	 * 
	 * @param p incoming value of pawn
	 */
	public void setPawn(Pawn p)
	{
		if(p == null){
			_list.removeAll(_list);
			this.setText("");
		}
		else {
			_list.add(p);
			String names = "";
			if(!_list.isEmpty()){
				for(Pawn pawn : _list){
					names = names+pawn.getPlayerName();
				}
				this.setText(names);
			}
			
		}
		
//		_pawn=p;
//		_list.add(p);
//		if (_pawn!=null){
//			if (this.getText() != "") {this.setText(this.getText()+ ", " +_pawn.getPlayerName());}
//			else this.setText(_pawn.getPlayerName());
			this.setHorizontalTextPosition(JLabel.CENTER);
//		}
	}
	
	/**
	 * setPawnList(HashSet<Pawn> p) sets the text of a players name on to the Tile
	 * 
	 * @param p HashSet of possible player pawns on the Tile
	 */
	public void setPawnList(HashSet<Pawn> p){
		_list.addAll(p);
		String names = "";
		for(Pawn pawn : _list){
			this.setText(this.getText()+pawn.getPlayerName());
		}
//		this.setText(null);
//		this.setText(names);
		System.out.println(this.getText());
		
	}
	
	/**
	 * getPawnList() returns the HashSet of strings representing the players names on that tile
	 * 
	 * @return HashSet of Strings 
	 */
	public HashSet<Pawn> getPawnList(){
		return _list;
	}
	
	/**
	 * removePawn(Pawn p): has a parameter of the pawn to be removed. This method searches for the players name from
	 *                     the HashSet, removes it, and relabels the tile with the players still on
	 * 
	 * @param p pawn to be removed
	 */
	public void removePawn(Pawn p){
		_list.remove(p);
		String names = "";
		for(Pawn pawn : _list){
			names = names+pawn.getPlayerName();
		}
		this.setText(names);
		System.out.println(this.getText());
		revalidate();
		repaint();
	}
	
	/**
	 * getPawn()
	 * 
	 * @return the players pawn
	 */
	public Pawn getPawn()
	{
		return _pawn;
	}
	
	/**
	 * getRow() returns the integer value for the row the Tile is currently in
	 * 
	 * @return the integer value of the row
	 */
	public int getRow()
	{
		return _row;
	}
	
	/**
	 * getColumn() returns the integer value of the column the Tile is currently in
	 * 
	 * @return the integer value of the column
	 */
	public int getColumn()
	{
		return _column;
	}
	
	/**
	 * setRow() sets the instance of the variable _row of a Tile 
	 * 
	 * @param r the row to be set in the Tile
	 */
	public void setRow(int r)
	{
		_row=r;
	}
	
	/**
	 * setColumn() sets the instance variable _column of the Tile
	 * 
	 * @param c the column to be set in the Tile
	 */
	public void setColumn(int c)
	{
		_column=c;
	}
	
	/**
	 * setNorth(boolean n) sets the boolean value of the top of the Tile
	 * 
	 * @param n boolean value 
	 */
	public void setNorth(boolean n){
		_north = n;
	}
	
	/**
	 * setNorth(boolean n) sets the boolean value of the right of the Tile
	 * 
	 * @param n boolean value 
	 */
	public void setEast(boolean e){
		_east = e;
	}
	
	/**
	 * setNorth(boolean n) sets the boolean value of the South of the Tile
	 * 
	 * @param n boolean value 
	 */
	public void setSouth(boolean s){
		_south = s;
	}
	
	/**
	 * setNorth(boolean n) sets the boolean value of the west of the Tile
	 * 
	 * @param n boolean value 
	 */
	public void setWest(boolean w){
		_west = w;
	}
	
	/**
	 * setToken(boolean b, int i): with parameters boolean and int, sets the image of the tile using the integer value
	 *                             and using the boolean value, if there is a token on the Tile set the image of the token 
	 *                             on the tile
	 * 
	 * @param b boolean value, if the tile has a token
	 * @param i integer value, what kind of tile is it
	 */
	public void setToken(boolean b, int i){
		_hasToken = b;
		//_token = new Token(i);
		switch(i){
		case 1: {
				JLabel innerLabel = new JLabel();
				ImageIcon icon = new ImageIcon("src/images/token_One.png");
				setTokenIcon(icon, innerLabel);
				break;
		}
		case 2: {
			JLabel innerLabel = new JLabel();
			ImageIcon icon = new ImageIcon("src/images/token_Two.png");
			setTokenIcon(icon, innerLabel);
			break;
		}
		case 3: {
			JLabel innerLabel = new JLabel();
			ImageIcon icon = new ImageIcon("src/images/token_Three.png");
			setTokenIcon(icon, innerLabel);
			break;
		}
		case 4: {
			JLabel innerLabel = new JLabel();
			ImageIcon icon = new ImageIcon("src/images/token_Four.png");
			setTokenIcon(icon, innerLabel);
			break;
		}
		case 5: {
			JLabel innerLabel = new JLabel();
			ImageIcon icon = new ImageIcon("src/images/token_Five.png");
			setTokenIcon(icon, innerLabel);
			break;
		}
		case 6: {
			JLabel innerLabel = new JLabel();
			ImageIcon icon = new ImageIcon("src/images/token_Six.png");
			setTokenIcon(icon, innerLabel);
			break;
		}
		case 7: {
			JLabel innerLabel = new JLabel();
			ImageIcon icon = new ImageIcon("src/images/token_Seven.png");
			setTokenIcon(icon, innerLabel);
			break;
		}
		case 8: {
			JLabel innerLabel = new JLabel();
			ImageIcon icon = new ImageIcon("src/images/token_Eight.png");
			setTokenIcon(icon, innerLabel);
			break;
		}
		case 9: {
			JLabel innerLabel = new JLabel();
			ImageIcon icon = new ImageIcon("src/images/token_Nine.png");
			setTokenIcon(icon, innerLabel);
			break;
		}
		case 10: {
			JLabel innerLabel = new JLabel();
			ImageIcon icon = new ImageIcon("src/images/token_Ten.png");
			setTokenIcon(icon, innerLabel);
			break;
		}
		case 11: {
			JLabel innerLabel = new JLabel();
			ImageIcon icon = new ImageIcon("src/images/token_11.png");
			setTokenIcon(icon, innerLabel);
			break;
		}
		case 12: {
			JLabel innerLabel = new JLabel();
			ImageIcon icon = new ImageIcon("src/images/token_12.png");
			setTokenIcon(icon, innerLabel);
			break;
		}
		case 13: {
			JLabel innerLabel = new JLabel();
			ImageIcon icon = new ImageIcon("src/images/token_13.png");
			setTokenIcon(icon, innerLabel);
			break;
		}
		case 14: {
			JLabel innerLabel = new JLabel();
			ImageIcon icon = new ImageIcon("src/images/token_14.png");
			setTokenIcon(icon, innerLabel);
			break;
		}
		case 15: {
			JLabel innerLabel = new JLabel();
			ImageIcon icon = new ImageIcon("src/images/token_15.png");
			setTokenIcon(icon, innerLabel);
			break;
		}
		case 16: {
			JLabel innerLabel = new JLabel();
			ImageIcon icon = new ImageIcon("src/images/token_16.png");
			setTokenIcon(icon, innerLabel);
			break;
		}
		case 17: {
			JLabel innerLabel = new JLabel();
			ImageIcon icon = new ImageIcon("src/images/token_17.png");
			setTokenIcon(icon, innerLabel);
			break;
		}
		case 18: {
			JLabel innerLabel = new JLabel();
			ImageIcon icon = new ImageIcon("src/images/token_18.png");
			setTokenIcon(icon, innerLabel);
			break;
		}
		case 19: {
			JLabel innerLabel = new JLabel();
			ImageIcon icon = new ImageIcon("src/images/token_19.png");
			setTokenIcon(icon, innerLabel);
			break;
		}
		case 20: {
			JLabel innerLabel = new JLabel();
			ImageIcon icon = new ImageIcon("src/images/token_20.png");
			setTokenIcon(icon, innerLabel);
			break;
		}
		case 25: {
			JLabel innerLabel = new JLabel();
			ImageIcon icon = new ImageIcon("src/images/token_25.png");
			setTokenIcon(icon, innerLabel);
			break;
		}
			
		}
		
	}
	
	/**
	 * setToken(Token t): sets a token on the tile
	 * 
	 * @param theToken instance of Token class
	 */
	public void setToken(Token theToken)
	{
		_token = theToken;
		if(theToken != null){
			setToken(true, theToken.getValue());
		} else {
			this.removeAll();
			_hasToken = false;
		}
	}
	
	/**
	 * setTokenIcon(ImageIcon icon, JLabel innerLabel): sets the image of a token and its label representation on
	 *                                                  the corresponding tile
	 * 
	 * @param icon image of token to be set
	 * @param innerLabel token
	 */
	public void setTokenIcon(ImageIcon icon, JLabel innerLabel){
		Image img = icon.getImage();
		Image sizedimg = img.getScaledInstance(20, 20, java.awt.Image.SCALE_SMOOTH);
		ImageIcon sizedIcon = new ImageIcon(sizedimg);
		innerLabel.setIcon(sizedIcon);
		this.add(innerLabel);
		this.setLayout(new FlowLayout());
	}
	
	/**
	 * If the tile has a token returns true, false otherwise
	 * 
	 * @return boolean value
	 */
	public boolean hasToken()
	{
		return _hasToken;
	}
	
	/**
	 * returns a token
	 * 
	 * @return token to be returned
	 */
	public Token getToken()
	{
		return _token;
	}
}
