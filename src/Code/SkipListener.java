package Code;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseListener;

import javax.swing.JButton;

public class SkipListener implements ActionListener {
	
	private GUI _gui;
	private MouseListener _m;
	
	/**
	 * It's an ActionListener constructor, you know what it does
	 * 
	 * @param gui
	 * @param m
	 */
	public SkipListener(GUI gui, MouseListener m){
		_gui = gui;
		_m = m;
	}

	/**
	 * Allows a user to skip a turn, (prevents an error printing) 
	 * 
	 */
	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		_gui.breakDownGB(_m);
		_gui.setUpTurns();
		JButton b = (JButton) e.getSource();
		b.removeActionListener(this);
	}

}
