package Code;

import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

import com.sun.glass.events.MouseEvent;

import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionListener;
import java.awt.event.KeyListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseListener;
import java.util.HashSet;
import java.util.Random;

@SuppressWarnings("unused")
public class GUI implements MouseListener 
{
	private JFrame _gameWindow; 
	private JPanel _mainPanel;
	private JPanel _infoPanel;
	private JPanel _gameBWindow;
	private JPanel _freeTileWindow;
	private GameBoard _gb;
	private Tile[][] _tileList;
	private String[] _args;
	private int _turnCount, _tokenCount;
	private JButton _p1, _p2, _p3, _p4, _skip, _pickUpToken;
	private Pawn[] _pawns;
	private Tile _tileWithToken;
	private int _p1score, _p2score, _p3score, _p4score;
	private JLabel _scoreCount1, _scoreCount2, _scoreCount3, _scoreCount4;
	
	
	public GUI (GameBoard gb, String[] args, Pawn[] pawns)
	{
		_p1score = 0;
		_p2score = 0;
		_p3score = 0;
		_p4score = 0;
		_turnCount = 0;
		_tokenCount = 1;
		_args = args;
		_pawns = pawns;
		_gameWindow = new JFrame("aMazing");
		_infoPanel = new JPanel();
		_gameBWindow = new JPanel();
		_gb=gb;
		_tileList = _gb.getGameBoard();
		_mainPanel = (JPanel) _gameWindow.getContentPane();
		_mainPanel.setLayout(new BoxLayout(_mainPanel, BoxLayout.X_AXIS));
		setUpInfoPanel();
		_mainPanel.add(_infoPanel);
		_mainPanel.add(_gameBWindow);
		setUpGB();
		setUpPawns();
		setUpFreeTileWindow();
		_gameWindow.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		_gameWindow.setVisible(true);
		_gameWindow.pack();
		_gameBWindow.setVisible(true);
		setUpTurns();
	}
	
	public int getTokenCount(){
		return _tokenCount;
	}

	public void setUpPawns() {
		_tileList[2][2].setPawn(_pawns[0]);
		_tileList[2][4].setPawn(_pawns[1]);
		_tileList[4][2].setPawn(_pawns[2]);
		_tileList[4][4].setPawn(_pawns[3]);
	}

	public void setUpTurns() {
		_pickUpToken.setEnabled(false);
		if (_turnCount%4 == 0)
		{
			_p1.setEnabled(true);
			_p4.setEnabled(false);
			_p4.setText(_args[3]);
		}
		if (_turnCount%4 == 1)
		{
			_p1.setEnabled(false);
			_p1.setText(_args[0]);
			_p2.setEnabled(true);
		}
		if (_turnCount%4 == 2)
		{
			_p2.setEnabled(false);
			_p2.setText(_args[1]);
			_p3.setEnabled(true);
		}
		if (_turnCount%4 == 3)
		{
			_p3.setEnabled(false);
			_p3.setText(_args[2]);
			_p4.setEnabled(true);
		}
		_turnCount++;
	}

	private void setUpInfoPanel() {
		_scoreCount1 = new JLabel(_args[0] + " score: " + _p1score);
		_scoreCount2 = new JLabel(_args[1] + " score: " + _p2score);
		_scoreCount3 = new JLabel(_args[2] + " score: " + _p3score);
		_scoreCount4 = new JLabel(_args[3] + " score: " + _p4score);
		_scoreCount1.setHorizontalTextPosition(JLabel.CENTER);
		_scoreCount2.setHorizontalTextPosition(JLabel.CENTER);
		_scoreCount3.setHorizontalTextPosition(JLabel.CENTER);
		_scoreCount4.setHorizontalTextPosition(JLabel.CENTER);
		JButton p1 = new JButton(_args[0]);
		JButton p2 = new JButton(_args[1]);
		JButton p3 = new JButton(_args[2]);
		JButton p4 = new JButton(_args[3]);
		TurnHandler th = new TurnHandler(p1, p2, p3, p4, this);
		p1.addActionListener(th);
		p2.addActionListener(th);
		p3.addActionListener(th);
		p4.addActionListener(th);
		p1.setEnabled(false);
		p2.setEnabled(false);
		p3.setEnabled(false);
		p4.setEnabled(false);
		_p1 = p1;
		_p2 = p2;
		_p3 = p3;
		_p4 = p4;
		_skip = new JButton("End Turn");
		_skip.setEnabled(false);
		_pickUpToken = new JButton("Pick Up Token");
		_pickUpToken.setEnabled(false);
		_pickUpToken.addActionListener(new TokenPickUpHandler(this));
		_infoPanel.add(p1);
		_infoPanel.add(p2);
		_infoPanel.add(p3);
		_infoPanel.add(p4);
		_infoPanel.add(_skip);
		_infoPanel.add(_pickUpToken);
		_infoPanel.add(_scoreCount1);
		_infoPanel.add(_scoreCount2);
		_infoPanel.add(_scoreCount3);
		_infoPanel.add(_scoreCount4);
		_infoPanel.setLayout(new GridLayout(8,1));
	}

	/**
	 * pastes the free tile and enables the player to rotate the tile
	 */
	public void setUpFreeTileWindow(){
		_freeTileWindow = new JPanel();
		_freeTileWindow.setLayout(new BoxLayout(_freeTileWindow , BoxLayout.PAGE_AXIS));
		_freeTileWindow.add(_gb.getFreeTile());
		JButton rotato = new JButton("Rotate");
		_freeTileWindow.add(rotato);
		rotato.addActionListener(new RotateHandler(_gb));
		_freeTileWindow.setVisible(true);
		_gameWindow.add(_freeTileWindow);
	}
	
	public void setUpGB()
	{
		_gameBWindow.setLayout(new GridLayout(7,7));
		for(int i=0; i<7; i++)
		{
			for(int k=0; k<7; k++)
			{
				_gameBWindow.add(_tileList[i][k]);
			}
		}
		
	}
	
	public void setUpMouseListeners(MouseListener l){
		for(int i=0; i<7; i++)
		{
			for(int k=0; k<7; k++)
			{
				_tileList[i][k].addMouseListener(l);
			}
		}
	}
	
	public void breakDownGB(MouseListener l)
	{
		
		for(int i=0; i<7; i++)
		{
			for(int k=0; k<7; k++)
			{
				_tileList[i][k].removeMouseListener(l);
			}
		}
		_skip.setEnabled(false);
	}

	@Override
	public void mouseClicked(java.awt.event.MouseEvent e) {
		for(int i = 0; i<7; i++){
			for(int k = 0; k<7; k++){				
				if(e.getSource() == _tileList[i][k]) {
					if(_gb.pushTile(i, k)){
						updateGameWindow();
						breakDownGB(this);
						i=7;
						k=7;
						setUpPawnMovement();
					}
				}
			}
		}
		
	}

	/**
	 * sets up pawn movement and adds the ability to allow a player to skip their turn or move their pawn
	 */
	public void setUpPawnMovement() {
		PawnMoveListener p = new PawnMoveListener(this, _pawns[(_turnCount - 1)%4]);
		setUpMouseListeners(p);
		_skip.addActionListener(new SkipListener(this, p));
		_skip.setEnabled(true);
	}

	/**
	 * updates the game window after each players choice
	 */
	public void updateGameWindow() {
		_gameBWindow.removeAll();
		setUpGB();
		_freeTileWindow.removeAll();
		setUpFreeTileWindow();
		_gameWindow.revalidate();
		_gameWindow.repaint();
	}

	@Override
	public void mousePressed(java.awt.event.MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseReleased(java.awt.event.MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseEntered(java.awt.event.MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseExited(java.awt.event.MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	/**
	 * enables the skip button
	 * 
	 * @return skip JButton
	 */
	public Object getSkip() {
		// TODO Auto-generated method stub
		return _skip;
	}
	
	/**
	 * enables a player to pickup a token if the tile contains one
	 * 
	 * @param t tile
	 */
	public void setUpTokenPickUp(Tile t) {
		// TODO Auto-generated method stub
		if (t.hasToken()){
			if (t.getToken().getValue() == _tokenCount){
				_tileWithToken = t;
				_pickUpToken.setEnabled(true);
				_skip.setEnabled(true);
			}else setUpTurns();
		} else setUpTurns();
	}
	
	/**
	 * Allows a user to pick up a token and increases their score, if token number 25 is picked up the game force closes
	 * and the users scores are printed 
	 */
	public void pickUpToken(){
		Token t = _tileWithToken.getPawn().pickToken();
		switch ((_turnCount-1)%4){
		case 0: {
			_p1score = _p1score+t.getValue();
			_scoreCount1.setText(_args[0] + " score: " + _p1score);
			break;
		}
		case 1: {
			_p2score = _p2score+t.getValue();
			_scoreCount2.setText(_args[1] + " score: " + _p2score);
			break;
		}
		case 2: {
			_p3score = _p3score+t.getValue();
			_scoreCount3.setText(_args[2] + " score: " + _p3score);
			break;
		}
		case 3: {
			_p4score = _p4score+t.getValue();
			_scoreCount4.setText(_args[3] + " score: " + _p4score);
			break;
		}
		}
		updateGameWindow();
		setUpTurns();
		_skip.setEnabled(false);
		_tokenCount++;
		if(_tokenCount == 21){
			_tokenCount = 25;
		}
		if(_tokenCount == 26){
			_gameWindow.dispose();
			JFrame j = new JFrame();
			j.setLayout(new GridLayout(2,2));
			j.add(_scoreCount1);
			j.add(_scoreCount2);
			j.add(_scoreCount3);
			j.add(_scoreCount4);
			j.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			j.pack();
			j.setVisible(true);
		}
	}
}
