package Code;

import javax.swing.SwingUtilities;

public class Driver implements Runnable {
	
	private int _turnCount;
	private GameBoard _gb;
	private GUI _gui;
	private Pawn[]  _player;
	
		public static void main(String[] args)
		{
			SwingUtilities.invokeLater(new Driver(args));
		}
		

		/**
		 * Driver: creates an array of players and a new game board 
		 * 
		 * @param args string of names from command line
		 */
	public Driver(String[] args)
	{
		_player = new Pawn[4];
		_gb = new GameBoard(7,7);
		if (args.length==4){setCharacters(args);};
		
		
	}
	
	/**
	 * setCharacters(String[] args): A void method that sets the four player pawns in four specific locations on the game 
	 *                               board
	 * 
	 * @param args string from command line
	 */
	public void setCharacters(String[] args)
	{
		Tile[][] t = _gb.getGameBoard();
		
		_player[0]=new Pawn(t[2][2], null, _gb);
		_player[1]=new Pawn(t[2][4], null, _gb);
		_player[2]=new Pawn(t[4][2], null, _gb);
		_player[3]=new Pawn(t[4][4], null, _gb);
		for(int i = 0; i<4; i++)
		{
			_player[i].setPlayerName(args[i]);
			
		}
		_gui = new GUI(_gb, args, _player);

	}

	@Override
	public void run() {
		// TODO Auto-generated method stub
		
	}

}
