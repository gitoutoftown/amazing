package Code;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class RotateHandler implements ActionListener {
	public GameBoard _gb;
	
	/**
	 * RotateHandler(GameBoard gb): sets the instance variable of the game board
	 * 
	 * @param gb, instance of class gameBoard
	 */
	public RotateHandler(GameBoard gb){
		_gb = gb;
	}
	
	/**
	 * actionPerformed(ActionEvent e): calls method rotateFreeTile90() on the gameBoard rotating the free tile 90
	 */
	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		_gb.rotateFreeTile90();
		

	}

}
