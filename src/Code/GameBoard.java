/**
 * @author 
 * @version 1.00, 3/23/16
 */

package Code;
/*
 List of methods for GameBoard Class:
 
 GameBoard(int rows, int columns) - default constructor that takes in two arguments to create a 2D array
 
*/

import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;

import javax.swing.ImageIcon;

import Code.Tile;

//testone
public class GameBoard 
{
	
	private Tile[][] _gameBoardArray;
	private Tile _freeTile;
	private int _gameBoardRows, _gameBoardColumns;
	private ArrayList<Tile> _workingPathTileList= new ArrayList<Tile>(); 
	private ArrayList<Token> _possibleTokens = new ArrayList<Token>();
	private int _currentToken = 1;
	
	/**
	 * GameBoard() is the default constructor which creates a game board full of default tiles, 
	 * only used in testing
	 */
	public GameBoard()
	{
		_gameBoardRows=7;
		_gameBoardColumns=7;
		_gameBoardArray = new Tile[_gameBoardRows][_gameBoardColumns];
		for(int i=0; i<7; i++)
		{
			for(int k=0; k<7; k++)
			{
				_gameBoardArray[i][k]=new Tile(false, false, false, false, i, k);
			}
		}
		
	}
	
	/**
	 * Non-default constructor, creates a game board of the entered size 
	 * and calls the setUp method which generates tiles for the board
	 * 
	 * @param row specifies the integer value of the rows you want to set the game board with
	 * @param columns specifies the integer value of the columns you want to set the game board with
	 * 
	 * 
	 */
	public GameBoard(int rows, int columns) 
	{
		
		_gameBoardRows=rows;
		_gameBoardColumns=columns;
		_gameBoardArray = new Tile[_gameBoardRows][_gameBoardColumns];
		setUp();
		
	}
	
	/**
	 * setUp() creates a game board of random tiles and sets the random tiles coordinates, 
	 * a free tile is also created to be inserted
	 * 
	 */
	public void setUp()
	{
		Random r = new Random();
		for(int i=0; i<_gameBoardRows; i++)
		{
			for(int k=0; k<_gameBoardColumns; k++)
			{
				_gameBoardArray[i][k]=tileGen(r.nextInt(10), i, k);
			}
		}
		_freeTile=tileGen(r.nextInt(10),-1,-1);
	
		for(int tokenNum = 1; tokenNum < 22; tokenNum ++)
		{
			if(tokenNum<21)
			{
				_possibleTokens.add(new Token(tokenNum));
			}
			else if(tokenNum == 21)
			{
				_possibleTokens.add(new Token(25));
			}
		}
		
		Collections.shuffle(_possibleTokens);
		
		for(int rowT = 1; rowT < 6; rowT++)
		{
			for (int colT = 1; colT < 6; colT++)
			{
				if((rowT == 2 && colT == 2) || (rowT == 2 && colT == 4) || (rowT ==4 && colT == 2) || (rowT == 4 && colT ==4))
				{
					getTile(rowT, colT).setToken(null);
				}
				else
				{
					getTile(rowT, colT).setToken(_possibleTokens.get(0));
					_possibleTokens.remove(0);
				}
			}
		}
		
	}
	
	/**
	 * TileGen returns a new Tile object created from a switch statement, which selects one of 10 possible 
	 * Tile arrangements and sets the coordinates of the Tile's position in the game board. It also
	 * sets the Icon of each instance of the tiles with the image representing its NESW values
	 * 
	 * @param x randomly generated number used in the switch statement which selects the tile to be returned 
	 * @param i the coordinate of the tile in the row
	 * @param k the coordinate of the tile in the column
	 * @return the tile that was generated in the switch statement
	 */
	public Tile tileGen(int x,int i, int k)
	{
	
		switch (x)
		{
			case 0: {
				Tile tile = new Tile(true, false, true, false, i, k);
				tile.setIcon(new ImageIcon("src/images/North_South_Path.png"));
				return tile;
			}
			case 1: {
				Tile tile = new Tile(false, true, false, true, i, k);
				tile.setIcon(new ImageIcon("src/images/East_West_Path.png"));
				return tile;
			}
			case 2: {
				Tile tile = new Tile(true, true, false, false, i, k);
				tile.setIcon(new ImageIcon("src/images/North_East_Path.png"));
				return tile;
			}
			case 3: {
				Tile tile = new Tile(false, true, true, false, i, k);
				tile.setIcon(new ImageIcon("src/images/East_South_Path.png"));
				return tile;
			}
			case 4: {
				Tile tile = new Tile(false, false, true, true, i, k);
				tile.setIcon(new ImageIcon("src/images/South_West_Path.png"));
				return tile;
			}
			case 5: {
				Tile tile = new Tile(true, true, true, false, i, k);
				tile.setIcon(new ImageIcon("src/images/North_East_South_Path.png"));
				return tile;
			}
			case 6: {
				Tile tile = new Tile(false, true, true, true, i, k);
				tile.setIcon(new ImageIcon("src/images/East_West_South_Path.png"));
				return tile;
			}
			case 7: {
				Tile tile = new Tile(true, false, true, true, i, k);
				tile.setIcon(new ImageIcon("src/images/West_North_South_Path.png"));
				return tile;
			}
			case 8: {
				Tile tile = new Tile(true, false, false, true, i, k);
				tile.setIcon(new ImageIcon("src/images/North_West_Path.png"));
				return tile;
			}
			case 9: {
				Tile tile = new Tile(true, true, false, true, i, k);
				tile.setIcon(new ImageIcon("src/images/West_North_East_Path.png"));
				return tile;
			}
			default: return new Tile(true, false, true, false, i, k);
		}
	}
	
	/**
	 * pushTile pushes tiles in the selected row or column
	 * 
	 * @param r the integer value of the row that is being shifted
	 * @param c the column integer value of the column being shifted
	 * @return returns true if all the tiles were shifted false otherwise
	 */
	public boolean pushTile(int r, int c)
	{
		if(r==0&&(c%2!=0)) return topColumnPush(c);
		if((r==(_gameBoardRows-1))&&(c%2!=0)) return bottomColumnPush(c);
		if((r%2!=0)&&c==0) return leftRowPush(r);
		if((r%2!=0)&&(c==(_gameBoardColumns-1))) return rightRowPush(r);
		return false;
	}
	
	/**
	 * topColumnPush() takes the integer value of the column to be shifted and shifts it from the top
	 * 
	 * @param c the column selected to be shifted
	 * @return returns true if the column was pushed from the top, false otherwise
	 */
	public boolean topColumnPush(int c)
	{
		Tile tileToInsert = _freeTile;
		Tile nextTile;
		for(int i = 0; i<_gameBoardRows; i++)
		{
			nextTile=_gameBoardArray[i][c];
			_gameBoardArray[i][c]=tileToInsert;
			_gameBoardArray[i][c].setRow(i);//this line previously contained "setRow(i+=1)," which caused i to increment by two each iteration
			_gameBoardArray[i][c].setColumn(c);
			tileToInsert=nextTile;
		}
		setFreeTile(tileToInsert);
		if (_freeTile.getPawn()!=null)
		{
			_gameBoardArray[0][c].setPawnList(_freeTile.getPawnList());
			_freeTile.setPawn(null);
		}
		if (_freeTile.getToken() != null)
		{
			_gameBoardArray[0][c].setToken(_freeTile.getToken());
			_freeTile.setToken(null);
		}
		_freeTile.setText("");
		return true;
	}
	
	/**
	 * bottomColumnPush() takes the integer value of the column to be shifted and shifts it from the bottom
	 * 
	 * @param c the column selected to be shifted
	 * @return returns true if the column was pushed from the bottom, false otherwise
	 */
	public boolean bottomColumnPush(int c)
	{
		Tile tileToInsert = _freeTile;
		Tile nextTile;
		for(int i = _gameBoardRows-1; i>=0; i--)
		{
			nextTile=_gameBoardArray[i][c];
			_gameBoardArray[i][c]=tileToInsert;
			_gameBoardArray[i][c].setRow(i);
			_gameBoardArray[i][c].setColumn(c);
			tileToInsert=nextTile;
		}
		setFreeTile(tileToInsert);
		if (_freeTile.getPawn()!=null)
		{
			_gameBoardArray[_gameBoardRows-1][c].setPawnList(_freeTile.getPawnList());
			_freeTile.setPawn(null);
		}
		if (_freeTile.getToken() != null)
		{
			_gameBoardArray[_gameBoardRows-1][c].setToken(_freeTile.getToken());
			_freeTile.setToken(null);
		}
		_freeTile.setText("");
		return true;
	}
	
	/**
	 * leftRowPush() takes the integer value of the row to be shifted and shifts it from the left row
	 * 
	 * @param r the row selected to be shifted
	 * @return returns true if the row was pushed from the left, false otherwise
	 */
	public boolean leftRowPush(int r)
	{
		Tile tileToInsert = _freeTile;
		Tile nextTile;
		for(int i = 0; i<_gameBoardColumns; i++)
		{
			nextTile=_gameBoardArray[r][i];
			_gameBoardArray[r][i]=tileToInsert;
			_gameBoardArray[r][i].setColumn(i);
			_gameBoardArray[r][i].setRow(r);
			tileToInsert=nextTile;
		}
		setFreeTile(tileToInsert);
		if (_freeTile.getPawn()!=null)
		{
			_gameBoardArray[r][0].setPawnList(_freeTile.getPawnList());
			_freeTile.setPawn(null);
		}
		if (_freeTile.getToken() != null)
		{
			_gameBoardArray[r][0].setToken(_freeTile.getToken());
			_freeTile.setToken(null);
		}
		_freeTile.setText("");
		return true;
	}
	
	/**
	 * rightRowPush() takes the integer value of the row to be shifted and shifts it from the right row
	 * 
	 * @param r the row selected to be shifted
	 * @return returns true if the row was pushed from the right, false otherwise
	 */
	public boolean rightRowPush(int r)
	{
		Tile tileToInsert = _freeTile;
		Tile nextTile;
		for(int i = _gameBoardColumns-1; i>=0; i--)
		{
			nextTile=_gameBoardArray[r][i];
			_gameBoardArray[r][i]=tileToInsert;
			_gameBoardArray[r][i].setColumn(i);
			_gameBoardArray[r][i].setRow(r);
			tileToInsert=nextTile;
		}
		setFreeTile(tileToInsert);
		if (_freeTile.getPawn()!=null)
		{
			_gameBoardArray[r][_gameBoardColumns-1].setPawnList(_freeTile.getPawnList());
			_freeTile.setPawn(null);
		}
		if (_freeTile.getToken() != null)
		{
			_gameBoardArray[r][_gameBoardColumns-1].setToken(_freeTile.getToken());
			_freeTile.setToken(null);
		}
		_freeTile.setText("");
		return true;
	}
	
	/**
	 * getGameBoard() returns the game board, default constructor needs to be called
	 * 
	 * @return returns the game board that
	 */
	public Tile[][] getGameBoard()
	{
		return _gameBoardArray;
	}
	
	/**
	 * getFreeTile() returns the free tile, tile to be inserted into the game board
	 * 
	 * @return the free tile
	 */
	public Tile getFreeTile()
	{
		return _freeTile;
	}
	
	//Updates the _row and _column variables of the new free tile to -1
	/**
	 * setFreeTile() sets _freeTile from to the Tile from the parameter
	 * 
	 * @param t the tile that is set to the free tile
	 */
	public void setFreeTile(Tile t)
	{
		_freeTile=t;
		_freeTile.setColumn(-1);
		_freeTile.setRow(-1);
	}
	
	/**
	 * getTile, returns a tile with with the entered row and column
	 * 
	 * @param r row of the game board containing the tile
	 * @param c column of the game board containing the tile
	 * @return tile of specified coordinates
	 */
	public Tile getTile(int r, int c)
	{
		return _gameBoardArray[r][c];
	}
	/***
	 * setTile() method has a parameter of Tile, which is the tile to be placed in the game board, 
	 * and parameters of row and column, which is the coordinates in the game board the tile is to 
	 * be set
	 * 
	 * @param t tile to be set in the game board
	 * @param r row that the tile is to be placed
	 * @param c column that the tile is to be placed
	 */
	public void setTile(Tile t, int r, int c)
	{
		_gameBoardArray[r][c]=t;
		t.setRow(r);
		t.setColumn(c);
	}
	
	/**
	 * generatePath() has a parameter of Tile, this tile is tested to see if it is a possible pawn's movement.
	 * If it is a possible movement the tile is added to the path array 
	 * 
	 * @param t tile tested to see if it is a possible pawns move
	 * @return the updates path with tile if it was a possible pawns path
	 */
	public Path generatePath(Tile t)
	{
		Path tempPath = new Path();
		int r,c;
		Tile ogTile = t;
		_workingPathTileList.add(t);
		
		do
		{
			t = _workingPathTileList.get(0);
			r = t.getRow();
			c = t.getColumn();
			if(r+1<_gameBoardRows)
			{
				if((_gameBoardArray[r][c].getSouth()&&_gameBoardArray[r+1][c].getNorth())&&(!tempPath.contains(_gameBoardArray[r+1][c])))
				{
					_workingPathTileList.add(_gameBoardArray[r+1][c]);
					tempPath.add(_gameBoardArray[r+1][c]);
				}
			}
			
			if(r-1>=0)
			{
				if((_gameBoardArray[r][c].getNorth()&&_gameBoardArray[r-1][c].getSouth())&&(!tempPath.contains(_gameBoardArray[r-1][c])))
				{
					_workingPathTileList.add(_gameBoardArray[r-1][c]);
					tempPath.add(_gameBoardArray[r-1][c]);
				}
			}
			
			if(c+1<_gameBoardColumns)
			{
				if((_gameBoardArray[r][c].getEast()&&_gameBoardArray[r][c+1].getWest())&&(!tempPath.contains(_gameBoardArray[r][c+1])))
				{
					_workingPathTileList.add(_gameBoardArray[r][c+1]);
					tempPath.add(_gameBoardArray[r][c+1]);
				}
			}
			
			if(c-1>=0)
			{
				if((_gameBoardArray[r][c].getWest()&&_gameBoardArray[r][c-1].getEast())&&(!tempPath.contains(_gameBoardArray[r][c-1])))
				{
					_workingPathTileList.add(_gameBoardArray[r][c-1]);
					tempPath.add(_gameBoardArray[r][c-1]);
				}
			}
			_workingPathTileList.remove(t);
			
			
			
		}while(!_workingPathTileList.isEmpty()); 
		tempPath.remove(ogTile);
		return tempPath;
	}
		
	/**
	 * method rotateFreeTile90 uses the free tile as an argument, the board game has to be initialized, and rotates it 90 degrees to the right 
	 * 
	 * @param ft the current free tile
	 */
	public void rotateFreeTile90(){
		boolean newNorth = _freeTile.getWest();
		boolean newSouth = _freeTile.getEast();
		boolean newEast =_freeTile.getNorth();
		boolean newWest = _freeTile.getSouth();
		if(newNorth && newSouth && !newEast && !newWest) _freeTile.setIcon(new ImageIcon("src/images/North_South_Path.png"));
		else if(!newNorth && !newSouth && newEast && newWest) _freeTile.setIcon(new ImageIcon("src/images/East_West_Path.png"));
		else if(newNorth && !newSouth && newEast && !newWest) _freeTile.setIcon(new ImageIcon("src/images/North_East_Path.png"));
		else if(!newNorth && newSouth && newEast && !newWest) _freeTile.setIcon(new ImageIcon("src/images/East_South_Path.png"));
		else if(!newNorth && newSouth && !newEast && newWest) _freeTile.setIcon(new ImageIcon("src/images/South_West_Path.png"));
		else if(newNorth && !newSouth && !newEast && newWest) _freeTile.setIcon(new ImageIcon("src/images/North_West_Path.png"));
		else if(newNorth && !newSouth && newEast && newWest) _freeTile.setIcon(new ImageIcon("src/images/West_North_East_Path.png"));
		else if(!newNorth && newSouth && newEast && newWest) _freeTile.setIcon(new ImageIcon("src/images/East_West_South_Path.png"));
		else if(newNorth && newSouth && !newEast && newWest) _freeTile.setIcon(new ImageIcon("src/images/West_North_South_Path.png"));
		else if(newNorth && newSouth && newEast && !newWest) _freeTile.setIcon(new ImageIcon("src/images/North_East_South_Path.png"));
		_freeTile.setNorth(newNorth);
		_freeTile.setEast(newEast);
		_freeTile.setSouth(newSouth);
		_freeTile.setWest(newWest);
	}
	
	/**
	 * returns the int value of a token
	 * 
	 * @return int value of token
	 */
	public int getCurrentTokenValue()
	{
		return _currentToken;
	}
	
	/**
	 * increases the count of the number of tokens on the board by one
	 */
	public void increaseTokenByOne()
	{
		_currentToken = _currentToken + 1;
	}

}// end of class
	
