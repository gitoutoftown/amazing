package Tests;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.HashSet;

import org.junit.Test;
import Code.Path;
import Code.Tile;

public class PathTests {
	
	@Test public void addTest(){
		Path path = new Path();
		HashSet<Tile> expected = new HashSet<>();
		Tile t = new Tile(true, false, true, false, 0, 0);
		expected.add(t);
		path.add(t);
		HashSet<Tile> actual = path.returnList();
		assertTrue("I expected " + expected + " but got " + actual, expected.equals(actual));
	}
	
	@Test public void containsTest(){
		Path path = new Path();
		Tile t = new Tile(true, false, true, false, 0, 0);
		path.add(t);
		assertTrue("I expected true but got false", path.contains(t));
	}

}
