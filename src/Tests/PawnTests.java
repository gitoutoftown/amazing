package Tests;

import static org.junit.Assert.*;

import org.junit.Test;

import Code.GameBoard;
import Code.Pawn;
import Code.Tile;

public class PawnTests {
	
	@Test public void movePawnTest1(){
		GameBoard gb = new GameBoard(7,7);
		Tile[][] t = gb.getGameBoard();
		t[0][0] = new Tile(true, true, true, true, 0,0);
		t[0][1] = new Tile(true, true, true, true, 0,1);
		Pawn p = new Pawn(t[0][0], null, gb);
		t[0][0].setPawn(p);
		p.movePawn(t[0][1]);
		assertTrue("The tile did not contain " + p, t[0][1].getPawn() == p);
	}
	
	@Test public void movePawnTest2(){
		GameBoard gb = new GameBoard(7,7);
		Tile[][] t = gb.getGameBoard();
		t[0][0] = new Tile(true, true, true, true, 0,0);
		t[0][1] = new Tile(true, true, true, true, 0,1);
		Pawn p = new Pawn(t[0][0], null, gb);
		t[0][0].setPawn(p);
		p.movePawn(t[0][1]);
		assertTrue("The original tile still contained " + p, t[0][0].getPawn() == null);
	}
	
	@Test public void movePawnTestFail(){
		GameBoard gb = new GameBoard(7,7);
		Tile[][] t = gb.getGameBoard();
		t[0][0] = new Tile(true, true, true, true, 0,0);
		t[0][1] = new Tile(false, false, false, false, 0,1);
		Pawn p = new Pawn(t[0][0], null, gb);
		t[0][0].setPawn(p);
		assertFalse("The method returned true " + p, p.movePawn(t[0][1]));
	}
}
