package Tests;

import static org.junit.Assert.*;

import org.junit.Test;
import Code.Tile;
import Code.GameBoard;

public class TileRotationTest {

	@Test
	public void test() {
		boolean result = false;
		GameBoard gb = new GameBoard();
		Tile expected = new Tile(true, true, false, false, -1,-1);
		Tile actual = new Tile(false, false, true, true, -1,-1);
		gb.setFreeTile(expected);
		gb.rotateFreeTile90();
		actual = gb.getFreeTile();
		
		if((actual.getNorth()==expected.getNorth())&&(actual.getSouth()==expected.getSouth())
			&&(actual.getEast()==expected.getEast())&&(actual.getWest()==expected.getWest())){
			result = true;
		}
		assertTrue(" ",result);
	}
	
	@Test
	public void testOne() {
		boolean result = false;
		GameBoard gb = new GameBoard();
		Tile expected = new Tile(true, true, true, false, -1,-1);
		Tile actual = new Tile(false, true, true, true, -1,-1);
		gb.setFreeTile(expected);
		gb.rotateFreeTile90();
		actual = gb.getFreeTile();
		
		if((actual.getNorth()==expected.getNorth())&&(actual.getSouth()==expected.getSouth())
			&&(actual.getEast()==expected.getEast())&&(actual.getWest()==expected.getWest())){
			result = true;
		}
		assertTrue(" ",result);
	}
	
	@Test
	public void testTwo() {
		boolean result = false;
		GameBoard gb = new GameBoard();
		Tile expected = new Tile(true, true, true, false, -1,-1);
		Tile actual = new Tile(true, false, true, true, -1,-1);
		gb.setFreeTile(expected);
		gb.rotateFreeTile90();
		gb.rotateFreeTile90();
		actual = gb.getFreeTile();
		
		if((actual.getNorth()==expected.getNorth())&&(actual.getSouth()==expected.getSouth())
			&&(actual.getEast()==expected.getEast())&&(actual.getWest()==expected.getWest())){
			result = true;
		}
		assertTrue(" ",result);
	}
	
	@Test
	public void testThree() {
		boolean result = false;
		GameBoard gb = new GameBoard();
		Tile expected = new Tile(true, false, true, false, -1,-1);
		Tile actual = new Tile(false, true, false, true, -1,-1);
		gb.setFreeTile(expected);
		gb.rotateFreeTile90();
		actual = gb.getFreeTile();
		
		if((actual.getNorth()==expected.getNorth())&&(actual.getSouth()==expected.getSouth())
			&&(actual.getEast()==expected.getEast())&&(actual.getWest()==expected.getWest())){
			result = true;
		}
		assertTrue(" ",result);
	}
	
	@Test
	public void testfour() {
		boolean result = false;
		GameBoard gb = new GameBoard();
		Tile expected = new Tile(true, false, true, false, -1,-1);
		Tile actual = new Tile(true, false, true, false, -1,-1);
		gb.setFreeTile(expected);
		gb.rotateFreeTile90();
		gb.rotateFreeTile90();
		actual = gb.getFreeTile();
		
		
		if((actual.getNorth()==expected.getNorth())&&(actual.getSouth()==expected.getSouth())
			&&(actual.getEast()==expected.getEast())&&(actual.getWest()==expected.getWest())){
			result = true;
		}
		assertTrue(" ",result);
	}
	
	@Test
	public void testfive() {
		boolean result = false;
		GameBoard gb = new GameBoard();
		Tile expected = new Tile(true, false, true, false, -1,-1);
		Tile actual = new Tile(true, false, true, false, -1,-1);
		gb.setFreeTile(expected);
		gb.rotateFreeTile90();
		gb.rotateFreeTile90();
		gb.rotateFreeTile90();
		actual = gb.getFreeTile();
		
		
		if((actual.getNorth()==expected.getNorth())&&(actual.getSouth()==expected.getSouth())
			&&(actual.getEast()==expected.getEast())&&(actual.getWest()==expected.getWest())){
			result = true;
		}
		assertTrue(" ",result);
	}

}
