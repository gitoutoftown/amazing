package Tests;

import static org.junit.Assert.*;

import org.junit.Test;

import Code.GameBoard;
import Code.Path;
import Code.Pawn;
import Code.Tile;

public class GameBoardTests {
	
	@Test
	public void nullGameBoard(){
		//Tests to see if null tiles were created for the default constructor 
		GameBoard gb = new GameBoard();
		boolean baseTile = true;
		String result = "";
		Tile boardTile;  
		 
		 for(int r =0; r<7;r++){
				for(int c=0;c<7;c++){
					boardTile = gb.getTile(r, c);
					if(boardTile.getEast()!=false&& boardTile.getNorth()!=false&&boardTile.getSouth()!=false &&boardTile.getWest()!=false){
						baseTile = false;
					
					}
				}	
			}
		 

			if(baseTile){
				result="True";
			}
			else
				result = "False";
			
			assertTrue("I expect all the tiles to be null, the statement is: " + result ,baseTile);
		
	}
	
	@Test
	public void gameBoardGen(){
		 //tests to see if no null tile was created 
		// Second constructor used
		 GameBoard gb = new GameBoard(7,7);
		 boolean noNullTile = true;
		 String result = "";
		 Tile boardTile;
		
		for(int r =0; r<7;r++){
			for(int c=0;c<7;c++){
				boardTile = gb.getTile(r, c);
				if(boardTile == null){
					noNullTile = false;
				}
			}	
		}
		
		if(noNullTile){
			result="True";
		}
		else
			result = "False";
		
		assertTrue("I expect all the tiles to be null, the statement is: " + result ,noNullTile);
	}
	
	
	@Test public void tileGenTest(){
		GameBoard gb = new GameBoard(7,7);
		Tile expected = new Tile(true, false, true, false, 0, 0);
		Tile actual = gb.tileGen(0, 0, 0);
		assertTrue("I expected " + expected + " but got " + actual, actual.getNorth() && actual.getSouth() && !actual.getEast() && !actual.getWest());
	}
	
	@Test public void topColumnPushTest(){
		GameBoard gb = new GameBoard(7,7);
		Tile[][] ta = gb.getGameBoard();
		Tile expected = ta[1][1];
		gb.pushTile(0, 1);
		Tile actual = ta[2][1];
		assertTrue("I expected " + expected + " but got " + actual, actual == expected);
		}
	
	//Tests that the free tile is placed in the first spot relative to the direction the column is being pushed
	@Test public void freeTileTest(){
		GameBoard gb = new GameBoard(7,7);
		Tile[][] ta = gb.getGameBoard();
		Tile expected = new Tile(true, false, true, false, -1, -1);
		gb.setFreeTile(expected);
		gb.pushTile(0, 1);
		Tile actual = ta[0][1];
		assertTrue("I expected " + expected + " but got " + actual, actual == expected);
	}
	
	//Tests that the bottom tile becomes the free tile when the column is pushed down
	@Test public void bottomTileFreedomTest(){
		GameBoard gb = new GameBoard(7,7);
		Tile[][] ta = gb.getGameBoard();
		Tile expected = new Tile(true, false, true, false, 6, 1);
		ta[6][1] = expected;
		gb.pushTile(0, 1);
		Tile actual = gb.getFreeTile();
		assertTrue("I expected " + expected + " but got " + actual, actual == expected);
	}
	
	@Test public void bottomColumnPushTest(){
		GameBoard gb = new GameBoard(7,7);
		Tile[][] ta = gb.getGameBoard();
		Tile expected = ta[6][1];
		gb.pushTile(6, 1);
		Tile actual = ta[5][1];
		assertTrue("I expected " + expected + " but got " + actual, actual == expected);
		}
	
	@Test public void leftRowPushTest(){
		GameBoard gb = new GameBoard(7,7);
		Tile[][] ta = gb.getGameBoard();
		Tile expected = ta[1][0];
		gb.pushTile(1, 0);
		Tile actual = ta[1][1];
		assertTrue("I expected " + expected + " but got " + actual, actual == expected);
		}
	
	@Test public void rightRowPushTest(){
		GameBoard gb = new GameBoard(7,7);
		Tile[][] ta = gb.getGameBoard();
		Tile expected = ta[1][6];
		gb.pushTile(1, 6);
		Tile actual = ta[1][5];
		assertTrue("I expected " + expected + " but got " + actual, actual == expected);
		}
	
	@Test public void pathGenerateTest1Right(){
		GameBoard gb = new GameBoard(7,7);
		Tile[][] ta = gb.getGameBoard();
		Tile a = new Tile(true, true, true, true, 0, 1);
		Tile b = new Tile(true, true, true, true, 0, 2);
		ta[0][1] = a;
		ta[0][2] = b;
		Path p = gb.generatePath(a);
		assertTrue("The path did not contain " + b, p.contains(b));
	}
	
	//Fails
	@Test public void pathGenerateTest2Right(){
		GameBoard gb = new GameBoard(7,7);
		Tile[][] ta = gb.getGameBoard();
		Tile a = new Tile(true, true, true, true, 0, 1);
		Tile b = new Tile(true, true, true, true, 0, 3);
		Tile c = new Tile(true, true, true, true, 0, 2);
		ta[0][1] = a;
		ta[0][3] = b;
		ta[0][2] = c;
		Path p = gb.generatePath(a);
		assertTrue("The path did not contain " + b, p.contains(b));
	}
	
	@Test public void pathGenerateTest2Down(){
		GameBoard gb = new GameBoard(7,7);
		Tile[][] ta = gb.getGameBoard();
		Tile a = new Tile(true, true, true, true, 0, 1);
		Tile b = new Tile(true, true, true, true, 2, 1);
		Tile c = new Tile(true, true, true, true, 1, 1);
		ta[0][1] = a;
		ta[2][1] = b;
		ta[1][1] = c;
		Path p = gb.generatePath(a);
		assertTrue("The path did not contain " + b, p.contains(b));
	}
	
	@Test public void pathGenerateTest2Up(){
		GameBoard gb = new GameBoard(7,7);
		Tile[][] ta = gb.getGameBoard();
		Tile a = new Tile(true, true, true, true, 0, 1);
		Tile b = new Tile(true, true, true, true, 2, 1);
		Tile c = new Tile(true, true, true, true, 1, 1);
		ta[0][1] = a;
		ta[2][1] = b;
		ta[1][1] = c;
		Path p = gb.generatePath(b);
		assertTrue("The path did not contain " + a, p.contains(a));
	}
	
	@Test public void pathGenerateTest2Left(){
		GameBoard gb = new GameBoard(7,7);
		Tile[][] ta = gb.getGameBoard();
		Tile a = new Tile(true, true, true, true, 0, 1);
		Tile b = new Tile(true, true, true, true, 0, 3);
		Tile c = new Tile(true, true, true, true, 0, 2);
		ta[0][1] = b;
		ta[0][3] = a;
		ta[0][2] = c;
		Path p = gb.generatePath(b);
		assertTrue("The path did not contain " + a, p.contains(a));
	}
	
	@Test public void pathGenerateTestEdge(){
		GameBoard gb = new GameBoard();
		Tile[][] ta = gb.getGameBoard();
		Tile a = new Tile(true, true, true, true, 6, 6);
		ta[6][6] = a;
		Path p = gb.generatePath(a);
		assertFalse("The path contained " + a, p.contains(a));
	}
	
	@Test public void pathGenerateTestEdgeAdd(){
		GameBoard gb = new GameBoard(7,7);
		Tile[][] ta = gb.getGameBoard();
		Tile a = new Tile(true, true, true, true, 5, 6);
		Tile b = new Tile(true, true, true, true, 6, 6);
		ta[6][6] = b;
		ta[5][6] = a;
		Path p = gb.generatePath(a);
		assertTrue("The path did not contain " + b, p.contains(b));
	}
	
	@Test public void pathGenerateTestEdgeAdd2(){
		GameBoard gb = new GameBoard(7,7);
		Tile[][] ta = gb.getGameBoard();
		Tile a = new Tile(true, true, true, true, 6, 5);
		Tile b = new Tile(true, true, true, true, 6, 6);
		ta[6][6] = b;
		ta[6][5] = a;
		Path p = gb.generatePath(a);
		assertTrue("The path did not contain " + b, p.contains(b));
	}
	
	@Test public void pathGenerateTest1Left(){
		GameBoard gb = new GameBoard(7,7);
		Tile[][] ta = gb.getGameBoard();
		Tile a = new Tile(true, true, true, true, 0, 1);
		Tile b = new Tile(true, true, true, true, 0, 0);
		ta[0][1] = a;
		ta[0][0] = b;
		Path p = gb.generatePath(a);
		assertTrue("The path did not contain " + b, p.contains(b));
	}
	
	@Test public void pathGenerateTest1Down(){
		GameBoard gb = new GameBoard(7,7);
		Tile[][] ta = gb.getGameBoard();
		Tile a = new Tile(true, true, true, true, 0, 1);
		Tile b = new Tile(true, true, true, true, 1, 1);
		ta[0][1] = a;
		ta[1][1] = b;
		Path p = gb.generatePath(a);
		assertTrue("The path did not contain " + b, p.contains(b));
	}
	
	@Test public void pathGenerateTest1Up(){
		GameBoard gb = new GameBoard(7,7);
		Tile[][] ta = gb.getGameBoard();
		Tile a = new Tile(true, true, true, true, 1, 1);
		Tile b = new Tile(true, true, true, true, 0, 1);
		ta[1][1] = a;
		ta[0][1] = b;
		Path p = gb.generatePath(a);
		assertTrue("The path did not contain " + b, p.contains(b));
	}
	
	@Test public void pathGenerateFourDirections(){
		GameBoard gb = new GameBoard();
		Tile[][] ta = gb.getGameBoard();
		Tile a = new Tile(true, true, true, true, 1, 1);
		Tile u = new Tile(true, true, true, true, 0, 1);
		Tile d = new Tile(true, true, true, true, 2, 1);
		Tile l = new Tile(true, true, true, true, 1, 0);
		Tile r = new Tile(true, true, true, true, 1, 2);
		ta[1][1] = a;
		ta[0][1] = u;
		ta[2][1] = d;
		ta[1][0] = l;
		ta[1][2] = r;
		Path p = gb.generatePath(a);
		assertTrue("The path did not contain all of the required tiles", p.contains(u)&&p.contains(d)&&p.contains(l)&&p.contains(r));
	}
	
	@Test public void pathGenerateFourDirectionsTimesTwo(){
		GameBoard gb = new GameBoard();
		Tile[][] ta = gb.getGameBoard();
		Tile a = new Tile(true, true, true, true, 2, 2);
		Tile u = new Tile(true, true, true, true, 1, 2);
		Tile u2 = new Tile(true, true, true, true, 0, 2);
		Tile d = new Tile(true, true, true, true, 3, 2);
		Tile d2 = new Tile(true, true, true, true, 4, 2);
		Tile l = new Tile(true, true, true, true, 2, 1);
		Tile l2 = new Tile(true, true, true, true, 2, 0);
		Tile r = new Tile(true, true, true, true, 2, 3);
		Tile r2 = new Tile(true, true, true, true, 2, 4);
		ta[2][2] = a;
		ta[1][2] = u;
		ta[0][2] = u2;
		ta[3][2] = d;
		ta[4][2] = d2;
		ta[2][1] = l;
		ta[2][0] = l2;
		ta[2][3] = r;
		ta[2][4] = r2;
		Path p = gb.generatePath(a);
		assertTrue("The path did not contain all of the required tiles", p.contains(u)&&p.contains(d)&&p.contains(l)&&p.contains(r)&&p.contains(u2)&&p.contains(d2)&&p.contains(l2)&&p.contains(r2));
	}
	
	@Test public void pathGenerateContainsOriginalTile(){
		GameBoard gb = new GameBoard(7,7);
		Tile[][] ta = gb.getGameBoard();
		Tile a = new Tile(true, true, true, true, 1, 1);
		Tile b = new Tile(true, true, true, true, 0, 1);
		ta[1][1] = a;
		ta[0][1] = b;
		Path p = gb.generatePath(a);
		assertFalse("The path contained " + a, p.contains(a));
	}
	
	@Test public void topColumnPushPawnMovementTest(){
		GameBoard gb = new GameBoard(7,7);
		Tile[][] ta = gb.getGameBoard();
		Tile expected = ta[6][1];
		Pawn p = new Pawn(expected, null, gb);
		expected.setPawn(p);
		gb.pushTile(0, 1);
		assertTrue("The pawn was not assigned the proper tile", ta[0][1].getPawn() == p);
	}
	
	@Test public void bottomColumnPushPawnMovementTest(){
		GameBoard gb = new GameBoard(7,7);
		Tile[][] ta = gb.getGameBoard();
		Tile expected = ta[0][1];
		Pawn p = new Pawn(expected, null, gb);
		expected.setPawn(p);
		gb.pushTile(6, 1);
		assertTrue("The pawn was not assigned the proper tile", ta[6][1].getPawn() == p);
		}
	
	@Test public void leftRowPushPawnMovementTest(){
		GameBoard gb = new GameBoard(7,7);
		Tile[][] ta = gb.getGameBoard();
		Tile expected = ta[1][6];
		Pawn p = new Pawn(expected, null, gb);
		expected.setPawn(p);
		gb.pushTile(1, 0);
		assertTrue("The pawn was not assigned the proper tile", ta[1][0].getPawn() == p);
		}
	
	@Test public void rightRowPushPawnMovementTest(){
		GameBoard gb = new GameBoard(7,7);
		Tile[][] ta = gb.getGameBoard();
		Tile expected = ta[1][0];
		Pawn p = new Pawn(expected, null, gb);
		expected.setPawn(p);
		gb.pushTile(1, 6);
		assertTrue("The pawn was not assigned the proper tile", ta[1][6].getPawn() == p);
		}
	
	@Test public void findPathAfterShift(){
		GameBoard gb = new GameBoard(7,7);
		Tile[][] ta = gb.getGameBoard();
		System.out.println(ta[6][1].getRow());
		ta[0][0].setPawn(new Pawn(ta[0][0], null, gb));
		ta[0][0].getPawn().findPath();
		//System.out.println("!!!!!!!!!!!"+ta[6][1].getRow());
		gb.pushTile(0, 1);
		//System.out.println("!!!!!!!!!!!!!"+ta[0][1].getRow());
	//	System.out.println("!!!!!!!!!!!!!!!!" +ta[6][1].getRow());
		ta[0][0].getPawn().findPath();
		assertTrue("this will never be false", true);
	}

}
