package code.model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;
import code.model.Token;
import code.gui.GameBoardGUI;

/**
 *  This class sets up and populates the game board with Fixed and Moveable tiles,
 *  and creates and populates the players in the game.  It handles much of the logic
 *  for deciding the legality of situations as they arise during a game, such as
 *  if it is legal for a player to move from a given origin tile to a destination tile and 
 *  if it is legal for a player to insert the shiftable tile (the extra tile not on the
 *  board after populating the board with 49 tiles) at a chosen tile position. 
 *  
 * @author Ian, Josh 03-16-16 3:20-3:40pm
 * 
 * Ken, Ian, Josh 03-16-16 8-11pm
 * Edited by Weijin, Ian on 03-18-16 7am-9am
 */

public class GameBoard {
	
	
	
	/**
	 * Current player in the game; only this player may make a move, insertShiftable or rotateShiftable, etc.
	 */
	public static Player CURRENTPLAYER;
	
	public static boolean GAMEOVER = false;
	
	/**
	 * index of currentPlayer in players[] array
	 */
	public static int currentPlayerIndex;
	
	/**
	 * true if the current player has ended its turn; false otherwise; is reset to false between
	 * switching current players
	 */
	private boolean _turnIsOver;
	
	/**
	 * ArrayList of type Token that holds references to all the tokens in the game
	 */
	private ArrayList<Token> _tokens; 
	
	/**
	 * The token the is currently collectible by players (given that it is face up).  Ex.: If the value of
	 * _currentTargetToken is 18, then this means tokens with values 1-17 has already been collected by players.
	 */
	private Token _currentTargetToken;
	
	/**
	 * 2-dimensional array of type AbstractTile[][].  Holds the references to MovableTiles
	 * and FixedTiles corresponding to tile pieces in the board game.
	 */
	private AbstractTile[][] _board;
	
	/**
	 * ArrayList that holds MoveableTile references corresponding to the MoveableTiles
	 * on the gameboard, and the single shiftable tile remaining in the arraylist after
	 * populating the board with tiles.
	 */
	private ArrayList<MoveableTile> _arrayOfMoveTiles;
	
	/**
	 * The number of players on the board/in the game.
	 */
	private int _numOfPlayers;
	
	/**
	 * An array of type Player[] holding the references to player objects in the game/on the board
	 */
	private Player[] _players;

	/**
	 * Observer of this GameBoard, of type GameBoardGUI
	 */
	private GameBoardGUI _observer;
	
	
	private ArrayList<FormulaCards> _listOfCards;
	
//	private int _turns;
	
	/** 
	 * Constructor of GameBoard class with the int numPlayers as a parameter.
	 * This constructor creates-
	 * a new 2d array of 7x7 that acts as the gameboard,
	 * an arraylist of moveabletiles 
	 * and the player(s) 
	 * which are also assigned to the instance variables.
	 * 
	 * _numOfPlayers is assigned to the numPlayers which in tells the player class how 
	 * many players there are
	 * 
	 * @param numPlayers refers to the number of players that will be on the board
	 * @author Ken, Ian, Josh
	 * @author Ian, Satya 04-10-16
	 */
	public GameBoard(int numPlayers){
		arrayOfFormula();// added by billy
		if(numPlayers < 0 || numPlayers > Player.maxNumberOfPlayers) {
			numPlayers = 4;
		}
			_board = new AbstractTile[7][7];
			_arrayOfMoveTiles = new ArrayList<MoveableTile>();
			_numOfPlayers = numPlayers;
			_players = new Player[_numOfPlayers];
			_tokens = new ArrayList<Token>();
			currentPlayerIndex = 0;
	}
	
	/**
	 * This constructor is the constructor that is used when the game is loaded with a previous
	 * save file. It (eventually) instantiates all of the instance variables that are instantiated
	 * in the standard constructor.
	 * @param data the save file (as a string)
	 */
	public GameBoard (String data)
	{
		
		currentPlayerIndex = 0;
		
		String playerData="";
		String gbData="";
		int i = 0;
		while(data.charAt(i)!=' ')
		{
			playerData = playerData + data.charAt(i);
			i++;
		}
		while (i<data.length())
		{
			gbData = gbData + data.charAt(i);
			i++;
		}
		
		playerSetup (playerData);
		
		tileSetup(gbData);
		
	}
	/**
	 * This method iterates through the string to turn the save file into actual info about the
	 * players
	 * @param playerData is the section of the string that represents the players
	 */
	public void playerSetup(String playerData)
	{
		int state = 0;
		String playerName = "";
		String playerColor = "";
		String temp = "";
		int [] formCardHolder = new int[3];
		int formIndex = 0;
		int wandCount=0;
		int playerCount = 0;
		FormulaCards formCard = new FormulaCards();
		ArrayList<Token> tokenHolder = new ArrayList<Token>();
		ArrayList<Player> playerHolder = new ArrayList<Player>();
			
		for(int i= 0; i < playerData.length(); i++)
		{
			switch(state)
			{
				case 0: if (playerData.charAt(i)=='['){state = 1;}
						break;
				case 1: if(playerData.charAt(i)!=','){playerName = playerName + playerData.charAt(i);}
						else{state = 2;}
						break;
				case 2: if(playerData.charAt(i)!= ','){playerColor = playerColor + playerData.charAt(i);}
						else{state = 3;}
						break;
				case 3: if(playerData.charAt(i)==','){state = 4;}
						else{
							wandCount = (int) playerData.charAt(i);
							state = 4;
						}
						break;
				case 4: if (playerData.charAt(i)=='['){state = 5;}
						break;
				case 5: if ((playerData.charAt(i)!=',')&&((playerData.charAt(i)!=']'))){temp = temp + playerData.charAt(i);}
						if(playerData.charAt(i)==','){
							formCardHolder[formIndex] = Integer.parseInt(temp);
							formIndex++;
							temp = "";
						}
						if(playerData.charAt(i)==']'){
							state = 6;
							temp = "";
							formCard = getFormCard(formCardHolder);
						}
						break;
				case 6: if (playerData.charAt(i) == ','){state = 7;}
						break;
				case 7: if (playerData.charAt(i) == '['){state = 8;}
						break;
				case 8: if ((playerData.charAt(i)!=',')&&((playerData.charAt(i)!=']'))){temp = temp + playerData.charAt(i);}
						if(playerData.charAt(i)==','){
							tokenHolder.add(tokenFinder(Integer.parseInt(temp)));
							temp = "";
						}
						if(playerData.charAt(i)==']'){
							state = 9;
							temp = "";
						}
						break;
				case 9: 
							playerHolder.add(new Player(playerName, playerColor, wandCount, formCard, tokenHolder));
							playerName = "";
							playerColor = "";
							formIndex = 0;
							wandCount = 0;
							tokenHolder.clear();
							state = 0;
							playerCount++;
						
						break;
							
				}
			}
		_numOfPlayers = playerCount;
		_players = new Player[_numOfPlayers];
		int indexPlayer = 0;
		System.out.println(playerCount);
		for(Player p : playerHolder)
		{
			
			System.out.println(p.toString());
			_players[indexPlayer++]=p;	
			p.setGameBoard(this);
		}
		CURRENTPLAYER = _players[0];
	}
	
	/**
	 * This method returns the token object corresponding to the value.  This method is used by the playerSetup method as 
	 * part of loading the game from a file.
	 * This method takes an int as an argument.
	 * @author Albert Cowie  
	 */
	public Token tokenFinder(int tokenValue)
	{
		switch (tokenValue){
			case 1:  return new Token(1, "Crab Apple");
			case 2:  return new Token(2, "Pine Cone");
			case 3:  return new Token(3, "Oak Leaf");
			case 4:  return new Token(4, "Oil of Black Slugs");
			case 5:  return new Token(5, "Four-leaf Clover");
			case 6:  return new Token(6, "Garlic");
			case 7:  return new Token(7, "Raven's Feather");
			case 8:  return new Token(8, "Henbane");
			case 9:  return new Token(9, "Spider");
			case 10: return new Token(10, "Skull");
			case 11: return new Token(11, "Magic Wand Made of Blindworm");
			case 12: return new Token(12, "Quartz");
			case 13: return new Token(13, "Toad");
			case 14: return new Token(14, "Fire Salamander");
			case 15: return new Token(15, "Weasel Spit");
			case 16: return new Token(16, "Silver Thistle");
			case 17: return new Token(17, "Snake");
			case 18: return new Token(18, "Emerald");
			case 19: return new Token(19, "Root of Mandrake");
			case 20: return new Token(20, "Black Rooster");
			case 25: return new Token(25, "Berries of Mistletoe");
			default: return new Token(25, "Berries of Mistletoe");
		}
	}
	/**
	 * This method returns a FormulaCards object corresponding to the value.  This method is used by the playerSetup method as 
	 * part of loading the game from a file.
	 * This method takes an int  array as an argument.
	 * @author Albert Cowie  
	 */
	public FormulaCards getFormCard(int[] formCardValue)
	{
		FormulaCards temp = new FormulaCards();
		if ((formCardValue[0]==11)&&(formCardValue[1]==3)&&(formCardValue[2]==14))
		{
			temp.AddNames("Magic Wand Made of Blindworm", "Oak leaf", "Fire Salamanders");
			temp.AddValue(11, 3, 14);
			return temp;
		}
		if ((formCardValue[0]==5)&&(formCardValue[1]==25)&&(formCardValue[2]==18))
		{
			temp.AddNames("Four-leaf clover", "Berries of Mistletoe", "Emerald");
			temp.AddValue(5, 25, 18);
			return temp;
		}
		if ((formCardValue[0]==19)&&(formCardValue[1]==7)&&(formCardValue[2]==15))
		{
			temp.AddNames("Root Of Mandrake", "Raven's feather", "Weasel Spit");
			temp.AddValue(19, 7, 15);
			return temp;
		}
		if ((formCardValue[0]==1)&&(formCardValue[1]==10)&&(formCardValue[2]==13))
		{
			temp.AddNames("Crab apple", "Skull moss", "Toad");
			temp.AddValue(1, 10, 13);
			return temp;
		}
		if ((formCardValue[0]==20)&&(formCardValue[1]==17)&&(formCardValue[2]==3))
		{
			temp.AddNames("Black Rooster", "Snake", "Oak leaf");
			temp.AddValue(20, 17, 3);
			return temp;
		}
		if ((formCardValue[0]==9)&&(formCardValue[1]==20)&&(formCardValue[2]==11))
		{
			temp.AddNames("Spider", "Black Rooster", "Magic Wand Made of Blindworm");
			temp.AddValue(9, 20, 11);
			return temp;
		}
		if ((formCardValue[0]==6)&&(formCardValue[1]==14)&&(formCardValue[2]==8))
		{
			temp.AddNames("Garlic", "Fire Salamander", "HenBane");
			temp.AddValue(6, 14, 8);
			return temp;
		}
		if ((formCardValue[0]==14)&&(formCardValue[1]==4)&&(formCardValue[2]==10))
		{
			temp.AddNames("Fire Salamander", "Oil of Black Slug", "Skull moss");
			temp.AddValue(14, 4, 10);
			return temp;
		}
		if ((formCardValue[0]==13)&&(formCardValue[1]==15)&&(formCardValue[2]==12))
		{
			temp.AddNames("Toad", "Weasel Spit", "Quartz");
			temp.AddValue(13, 15, 12);
			return temp;
		}
		if ((formCardValue[0]==15)&&(formCardValue[1]==2)&&(formCardValue[2]==4))
		{
			temp.AddNames("Weasel Spit", "Pine Cone", "Oil of Black slug");
			temp.AddValue(15, 2, 4);
			return temp;
		}
		if ((formCardValue[0]==4)&&(formCardValue[1]==13)&&(formCardValue[2]==20))
		{
			temp.AddNames("Oil of Black slug", "Toad", "Black Rooster");
			temp.AddValue(4, 13, 20);
			return temp;
		}
		if ((formCardValue[0]==10)&&(formCardValue[1]==12)&&(formCardValue[2]==16))
		{
			temp.AddNames("Skull moss", "Quartz", "Silver Thistle");
			temp.AddValue(10, 12, 16);
			return temp;
		}
		if ((formCardValue[0]==16)&&(formCardValue[1]==9)&&(formCardValue[2]==7))
		{
			temp.AddNames("Silver Thistle", "Spider", "Raven's feather");
			temp.AddValue(16, 9, 7);
			return temp;
		}
		if ((formCardValue[0]==2)&&(formCardValue[1]==8)&&(formCardValue[2]==17))
		{
			temp.AddNames("Pine cone", "Henbane", "Snake");
			temp.AddValue(2, 8, 17);
			return temp;
		}
		if ((formCardValue[0]==17)&&(formCardValue[1]==5)&&(formCardValue[2]==6))
		{
			temp.AddNames("Snake", "Four-leaf clover", "Garlic");
			temp.AddValue(17, 5, 6);
			return temp;
		}
		if ((formCardValue[0]==8)&&(formCardValue[1]==19)&&(formCardValue[2]==5))
		{
			temp.AddNames("HenBane", "Root of Mandrake", "Four-leaf clover");
			temp.AddValue(8, 19, 5);
			return temp;
		}
		if ((formCardValue[0]==5)&&(formCardValue[1]==18)&&(formCardValue[2]==1))
		{
			temp.AddNames("Four-leaf clover", "Emerald", "Crab Apple");
			temp.AddValue(5, 18, 1);
			return temp;
		}
		if ((formCardValue[0]==12)&&(formCardValue[1]==1)&&(formCardValue[2]==9))
		{
			temp.AddNames("Quartz", "Crab Apple", "Spider");
			temp.AddValue(12, 1, 9);
			return temp;
		}
		if ((formCardValue[0]==18)&&(formCardValue[1]==11)&&(formCardValue[2]==19))
		{
			temp.AddNames("Emerald", "Magic Wand Made of Blindworm", "Root of Mandrake");
			temp.AddValue(18, 11, 19);
			return temp;
		}
		if ((formCardValue[0]==25)&&(formCardValue[1]==16)&&(formCardValue[2]==2))
		{
			temp.AddNames("Berries of Mistletoe", "Silver Thistle", "Pine Cone");
			temp.AddValue(25, 16, 2);
			return temp;
		}
		if ((formCardValue[0]==7)&&(formCardValue[1]==6)&&(formCardValue[2]==25))
		{
			temp.AddNames("Raven's Feather", "Garlic", "Berries of Mistletoe");
			temp.AddValue(7, 6, 25);
			return temp;
		}
		return temp;
	}
	
	/**
	 * This method iterates through the string to turn the save file into an actual gameboard
	 * @param tileData is the section of the string that represents the tiles
	 */
	public void tileSetup(String tileData)
	{
		//System.out.println(tileData.charAt(1));
		int state = 0;
		ArrayList<MoveableTile> tiles = new ArrayList<MoveableTile>();
		ArrayList<String> color= new ArrayList<String>();
		int min = 25;
		String temp="";
		int tileIndex =0;
		int illegalTile = 0;
		int T = 0;
	    int I = 0;
	    int L = 0;
		for(int i = 1; i<tileData.length(); i++)
		{
			
			switch (state){
				case 0: if(tileData.charAt(i)=='['){state = 1;System.out.println(tileData.charAt(i));}
					 break;
				case 1: if(tileData.charAt(i)!=','){temp=temp+tileData.charAt(i);}
						if(tileData.charAt(i)==','){
							System.out.println(temp);
							tiles.add(restoreTile(temp));
							if(temp.charAt(0)=='T') T++;
							if(temp.charAt(0)=='L') L++;
							if(temp.charAt(0)=='I') I++;
							temp="";
							state = 2;
						}
						break;
				case 2: if(tileData.charAt(i)!=','){temp=temp+tileData.charAt(i);}
						if(tileData.charAt(i)==','){
							int val = Integer.parseInt(temp);
							if(val!=0){
								Token t = tokenFinder(Integer.parseInt(temp));
								tiles.get(tileIndex).setToken(t);
								if(val<min){
									min = val;
									_currentTargetToken = t;
								}
							}
							temp="";
							state = 3;
						}
						System.out.println("state 2");
						break;
				case 3: if(tileData.charAt(i)=='['){state =4;}
						System.out.println("state 3");
						break;
				case 4: if((tileData.charAt(i)!=',') && (tileData.charAt(i)!=']')){temp=temp+tileData.charAt(i);}
						if(tileData.charAt(i)==','){
							color.add(temp);
							temp = "";
						}
						if(tileData.charAt(i)==']'){
							if(temp.isEmpty()){state = 5;}
							else{
								color.add(temp);
								temp = "";
								addPlayerToTile(color, tiles.get(tileIndex));
								color.clear();
								state = 5;
							}
						}
						System.out.println("state 4");
						break;
				case 5: if(tileData.charAt(i)==']'){state = 6;}
				System.out.println(tileData.charAt(i));
						break;
				case 6: if(tileData.charAt(i)==','){
							state = 0;
							tileIndex++;
							System.out.println("state 6");
							}
				System.out.println("state 6");
						if(tileData.charAt(i)!=' '){state = 7;}
						break;
				case 7: state = 8;
						break;
				case 8: temp = temp + tileData.charAt(i);
						state = 1;
						System.out.println("state 8");
						break;
				}
			}
		//illegalTile = Integer.parseInt(temp);
		_board = new AbstractTile[7][7];
		int n = 0;
		_arrayOfMoveTiles = new ArrayList<MoveableTile>();
		addMoveableTilesToArrayList(tiles);
		MoveableTile freeTile = findFreeTile(T, I, L);
		for(int a = 0; a<7; a++){
			for(int b = 0; b<7; b++)
			{
				System.out.println("Rmoving: " + tiles.size());
				_board[a][b]=tiles.remove(0);
				n++;
			}
		}
		_arrayOfMoveTiles.add(0, freeTile);
	}
	
	/**
	 * This method checks to see which of the tile identities is does not have the proper amount
	 * on the gameboard, and returns a MoveableTile which is to become the freeTile
	 * @param t the amount of 'T' tiles
	 * @param i the amount of 'I' tiles
	 * @param l the amount of 'L' tiles
	 * @return the type of tile which is missing one tile on the board, the free tile
	 */
	private MoveableTile findFreeTile(int t, int i, int l) {
		// TODO Auto-generated method stub
		if(t<18) return new MoveableTile("T");
		if(i<13) return new MoveableTile("I");
		if(l<19) return new MoveableTile("L");
		return null;
	}
	
	/**
	 * This method adds only the moveable tiles to the _arrayOfMoveTiles
	 */
	private void addMoveableTilesToArrayList(ArrayList<MoveableTile> tiles) {
		// TODO Auto-generated method stub
		for(MoveableTile t: tiles)
		{
			System.out.println(t.toString());
		}
		_arrayOfMoveTiles.add(tiles.get(1));
		_arrayOfMoveTiles.add(tiles.get(3));
		_arrayOfMoveTiles.add(tiles.get(5));
		_arrayOfMoveTiles.add(tiles.get(7));
		_arrayOfMoveTiles.add(tiles.get(8));
		_arrayOfMoveTiles.add(tiles.get(9));
		_arrayOfMoveTiles.add(tiles.get(10));
		_arrayOfMoveTiles.add(tiles.get(11));
		_arrayOfMoveTiles.add(tiles.get(12));
		_arrayOfMoveTiles.add(tiles.get(13));
		_arrayOfMoveTiles.add(tiles.get(15));
		_arrayOfMoveTiles.add(tiles.get(17));
		_arrayOfMoveTiles.add(tiles.get(19));
		_arrayOfMoveTiles.add(tiles.get(21));
		_arrayOfMoveTiles.add(tiles.get(22));
		_arrayOfMoveTiles.add(tiles.get(23));
		_arrayOfMoveTiles.add(tiles.get(24));
		_arrayOfMoveTiles.add(tiles.get(25));
		_arrayOfMoveTiles.add(tiles.get(26));
		_arrayOfMoveTiles.add(tiles.get(27));
		_arrayOfMoveTiles.add(tiles.get(29));
		_arrayOfMoveTiles.add(tiles.get(31));
		_arrayOfMoveTiles.add(tiles.get(33));
		_arrayOfMoveTiles.add(tiles.get(35));
		_arrayOfMoveTiles.add(tiles.get(36));
		_arrayOfMoveTiles.add(tiles.get(37));
		_arrayOfMoveTiles.add(tiles.get(38));
		_arrayOfMoveTiles.add(tiles.get(39));
		_arrayOfMoveTiles.add(tiles.get(40));
		_arrayOfMoveTiles.add(tiles.get(41));
		_arrayOfMoveTiles.add(tiles.get(43));
		_arrayOfMoveTiles.add(tiles.get(45));
		_arrayOfMoveTiles.add(tiles.get(47));
		
	}
	
	/**
	 * This method creates new tiles based on the input string, which is the identity and the
	 * orientation of the tile.
	 * @param tile is a string that contains the orientation and identity of the tile
	 * @return a MoveableTile object
	 */
	public MoveableTile restoreTile(String tile)
	{
		MoveableTile temp = new MoveableTile();
		if(tile.equals("T0"))
		{
			temp = new MoveableTile("T");
			temp.rotate(180);
		}
		if(tile.equals("T1"))
		{
			temp = new MoveableTile("T");
			temp.rotate(90);
		}
		if(tile.equals("T2"))
		{
			temp = new MoveableTile("T");
		}
		if(tile.equals("T3"))
		{
			temp = new MoveableTile("T");
			temp.rotate(-90);
		}
		if(tile.equals("L0"))
		{
			temp = new MoveableTile("L");
			temp.rotate(-90);
		}
		if(tile.equals("L1"))
		{
			temp = new MoveableTile("L");
			temp.rotate(180);
		}
		if(tile.equals("L2"))
		{
			temp = new MoveableTile("L");
			temp.rotate(90);
		}
		if(tile.equals("L3"))
		{
			temp = new MoveableTile("L");
		}
		if(tile.equals("I0"))
		{
			temp = new MoveableTile("I");
			temp.rotate(90);
		}
		if(tile.equals("I1"))
		{
			temp = new MoveableTile("I");
		}
		return temp;
	}
	public void addPlayerToTile(ArrayList<String> color, AbstractTile tile)
	{
		for(String c : color)
		{
			for(int i = 0; i<_players.length; i++)
			{
				System.out.println(_players.toString());
				if(c.equals(_players[i].getColor())){tile.addPlayer(_players[i]);}
			}
		}
	}
	/**
	 * This method populates the board
	 * by calling on populateRandomMoveableTileArray,populateBoardWithFixedTiles,
	 * populateBoardWithMoveableTiles,createAndPlacePlayers methods which help setup the board.
	 * The movable tiles are randomized and placed on the board, 
	 * along with the fixed tiles and the players.  Finally, the
	 * _observer is updated (GameBoardGUI).
	 * @author Ian, Weijin 
	 * @author Ian, Satya 04-10-16
	 */
	public void setupRandomBoard(){
		populateRandomMoveableTileArray();
		populateBoardWithFixedTiles();
		populateBoardWithMoveableTiles();
		createAndPlacePlayers();
		assignFormulaCards();
		populateTokenArray();
		populateBoardWithTokensRandomly();
		CURRENTPLAYER = _players[0];
	}
	
	/**
	 * This method populates the static board by
	 * calling on populateBoardWithFixedTiles,
	 * populateBoardWithMoveableTiles,createAndPlacePlayers methods which setup the board.
	 * The board is populated with fixed and movable tiles that are placed
	 * at the same location each time the board is setup.  Finall, the
	 * _observer is updated (GameBoardGUI).
	 * @author Ken, Josh
	 * @author Ian, Satya 04-10-16
	 */
	public void setupStaticBoard(){
		populateBoardWithFixedTiles();
		populateBoardWithMoveableTiles();
		createAndPlacePlayers();
		
		populateTokenArray();
		populateStaticBoardWithTokens();
		CURRENTPLAYER = _players[0];
	}
	
	private void assignFormulaCards() {
		// TODO Auto-generated method stub
		for (int i = 0; i<_numOfPlayers; i++){
			_players[i].setFormulaCard(_listOfCards.get(0));
			_listOfCards.remove(0);
		}
	}
	/**
	 * This method ads tokens to arraylist
	 * @author Weijin, Ian 03-18-16
	 */
	public void populateTokenArray() {
		_tokens.add(new Token(1, "Crab Apple"));
		_tokens.add(new Token(2, "Pine Cone"));
		_tokens.add(new Token(3, "Oak Leaf"));
		_tokens.add(new Token(4, "Oil of Black Slugs"));
		_tokens.add(new Token(5, "Four-leaf Clover"));
		_tokens.add(new Token(6, "Garlic"));
		_tokens.add(new Token(7, "Raven's Feather"));
		_tokens.add(new Token(8, "Henbane"));
		_tokens.add(new Token(9, "Spider"));
		_tokens.add(new Token(10, "Skull Moss"));
		_tokens.add(new Token(11, "Magic Wand Made of Blindworm"));
		_tokens.add(new Token(12, "Quartz"));
		_tokens.add(new Token(13, "Toad"));
		_tokens.add(new Token(14, "Fire Salamander"));
		_tokens.add(new Token(15, "Weasel Spit"));
		_tokens.add(new Token(16, "Silver Thistle"));
		_tokens.add(new Token(17, "Snake"));
		_tokens.add(new Token(18, "Emerald"));
		_tokens.add(new Token(19, "Root of Mandrake"));
		_tokens.add(new Token(20, "Black Rooster"));
		_tokens.add(new Token(25, "Berries of Mistletoe"));
		
		_currentTargetToken = _tokens.get(0);
		System.out.println(_currentTargetToken);
		System.out.println(_tokens.get(0).toString());//initialize current token to token (1, "Crab Apple")
	}
	
	/**This is the tokens population for random board
	 * @author satya, Ken (April 3rd, 2016) (3:15 pm)
	 * @param index
	 */
	public void populateBoardWithTokensRandomly(){ 
		ArrayList<Token> tokens = new ArrayList<Token>();
		for(Token t: _tokens){
			tokens.add(t);
		}
		Collections.shuffle(tokens); 
		 for(int i =1; i<=5; i= i+2 ){
			 for(int j =1; j<=5; j++){
				 _board[i][j].setToken(tokens.get(0));
				 tokens.get(0).setTile(_board[i][j]);
				 tokens.remove(0);
			 }
		 }
		 for(int i = 2; i<=4; i= i+2 ){
			 for(int j =1; j<=5; j= j+2){
				 _board[i][j].setToken(tokens.get(0));
				 tokens.get(0).setTile(_board[i][j]);
				 tokens.remove(0);
			 }
		 }
	}
	/**This is the tokens population for static board
	 * @author satya, Ken (April 3rd, 2016) (4:20 pm)
	 * @param index
	 */
	public void populateStaticBoardWithTokens(){ 
		int t = 0; 
		for(int i =1; i<=5; i= i+2 ){
			 for(int j =1; j<=5; j++){
				 _board[i][j].setToken(_tokens.get(t));
				 _tokens.get(t).setTile(_board[i][j]);
				 //_tokens.remove(0);
				 t++;
			 }
		 }
		 for(int i = 2; i<=4; i= i+2 ){
			 for(int j =1; j<=5; j= j+2){
				 _board[i][j].setToken(_tokens.get(t));
				 _tokens.get(t).setTile(_board[i][j]);
				// _tokens.remove(0);
				 t++;
			 }
		 }
	}
	
	/**
	 * This method tries to get certain tokens
	 * @param index order of the tokens
	 * @return token in a specific position
	 * @author Ian, Ken
	 */
	public Token getToken(int index){
		return _tokens.get(index);
	}
	
	/**
	 * This method is looking for next token
	 * @return next target token 
	 * @author Ian, Ken
	 */
	public Token getCurrentTargetToken(){
		return _currentTargetToken;
	}
	
	/**
	 * This method is looking for next token
	 * @return the value of next token 
	 * @author Ian, Ken
	 */
	public int getCurrentTargetTokenValue(){
		return _currentTargetToken.getValue();
	}
	
	
	/**
	 * This method populates game board with fixed tiles (16) & rotate by appropriate angle
	 * The method instantiates 16 tiles of the FixedTile class that are then
	 * rotated by calling the .rotate method on each tile 
	 * 
	 * @author Ken, Ian
	 */
	public void populateBoardWithFixedTiles(){
		//populate game board with fixed tiles (16) & rotate by appropriate angle
		_board[0][0] = new FixedTile("L");
		_board[0][0].rotate(-90);
		_board[0][2] = new FixedTile("T");
		_board[0][2].rotate(180);
		_board[0][4] = new FixedTile("T");
		_board[0][4].rotate(180);
		_board[0][6] = new FixedTile("L");
		_board[0][6].rotate(180);
		
		_board[2][0] = new FixedTile("T");
		_board[2][0].rotate(-90);
		_board[2][2] = new FixedTile("T");
		_board[2][2].rotate(-90);
		_board[2][4] = new FixedTile("T");
		_board[2][4].rotate(180);
		_board[2][6] = new FixedTile("T");
		_board[2][6].rotate(90);
		
		_board[4][0] = new FixedTile("T");
		_board[4][0].rotate(-90);
		_board[4][2] = new FixedTile("T");
		_board[4][2].rotate(0);
		_board[4][4] = new FixedTile("T");
		_board[4][4].rotate(90);
		_board[4][6] = new FixedTile("T");
		_board[4][6].rotate(90);
		
		_board[6][0] = new FixedTile("L");
		_board[6][0].rotate(0);
		_board[6][2] = new FixedTile("T");
		_board[6][2].rotate(0);
		_board[6][4] = new FixedTile("T");
		_board[6][4].rotate(0);
		_board[6][6] = new FixedTile("L");
		_board[6][6].rotate(90);
	}
	
	/**
	 * This method populates the static board with the array of movable tiles 
	 * 
	 * @param aL is the array of movable tiles that are static 
	 * The location of the tiles are the same each time the board is created.
	 * 
	 * @author satya,Ken (03-19-16, 5:35pm)
	 */
	public void populateStaticMoveableTileArray(ArrayList<MoveableTile> al) {
		_arrayOfMoveTiles = al;
	}
	/** 
	 * The method is a getter for the board which returns the board
	 * 
	 * @return _board which is the 2d array of the AbstractTile class
	 * @author Ken, Ian
	 */
	public AbstractTile[][] getBoard(){
		return _board;
	}
	/**
	 * The method randomly populates board with movable tiles
	 * _arrayOfMoveTiles contains a total of 34 MoveableTile instances
	 * 6 T type tiles, 15 L type tiles, and 13 I type tiles
	 * These tiles are randomly rotated by calling .rotate(randomDegree()) 
	 * @author Ken, Ian
	 */
	public void populateRandomMoveableTileArray() {
		//_arrayOfMoveTiles contains a total of 34 MoveableTile instances
		//6 T type tiles, 15 L type tiles, and 13 I type tiles
		//The tiles are rotated at random to simulate them jumbled in a
		//bag, so they have random rotation on the board
		//there will be one MoveableTile left in the ArrayList
		//after using up 33 to populate the _board (of type AbstractTile[][])
	
		for(int i = 1; i <= 6; i++){
			MoveableTile mT = new MoveableTile("T");
			mT.rotate(randomDegree());
			_arrayOfMoveTiles.add(mT);
			
		}
		
		for(int i = 1; i <= 15; i++){
			MoveableTile mT = new MoveableTile("L");
			mT.rotate(randomDegree());
			_arrayOfMoveTiles.add(mT);
			
		}
		
		for(int i = 1; i <= 13; i++){
			MoveableTile mT = new MoveableTile("I");
			mT.rotate(randomDegree());
			_arrayOfMoveTiles.add(mT);
			
		}
		Collections.shuffle(_arrayOfMoveTiles);
		//System.out.println("Initial Size of array: " + _arrayOfMoveTiles.size());
	}
	/**
	 * The method creates a random int(0,90,-90,180) of degree for rotation
	 * @return returns a random degree which the .rotate method can 
	 * use to randomly rotate tiles
	 * 
	 * @author Weijin, Josh
	 */
	public int randomDegree(){
		int d = 0; //degree of rotation
		//Generate random int between 0 and 3 inclusive and map to appropriate degree
		//value of r is a member of set {0,90,-90,180}
		Random r = new Random();
		int randomDegree = r.nextInt(3);
		if(randomDegree == 0) {d = 0;}
		else if(randomDegree==1) {d = 90;}
		else if(randomDegree==2) {d = -90;}
		else if(randomDegree==3) {d = 180;}
		return d;
	}
	/**
	 * This method puts the movable tiles onto the board.
	 * The method uses a double for loop to set each tile on the board.
	 * by looping through the column(j) then the row(i).
	 * since the board has fixed tile the for loop for row(i) skips
	 * by two, only reaching the places where there isn't a tile.(1,3,5)
	 * Then after placing the tile it removes it from the arraylist
	 * so that the next tile  is the next item from the arraylist.
	 * Then once its done for the row it goes to the next column 
	 * continuing until it finishes row 7.
	 *
	 * 
	 * @author Ken, Ian
	 * @author Ken, Josh
	 */
	public void populateBoardWithMoveableTiles(){
		for(int i = 1; i <=5; i = i + 2){
			for(int j = 0; j < 7; j++){
				//int lastIndex = _arrayOfMoveTiles.size()-1;
				MoveableTile mT = _arrayOfMoveTiles.get(0);
				_board[j][i] = mT;
				_arrayOfMoveTiles.remove(mT);
			}
		}		

		for(int i = 1; i <=5; i = i + 2){
			for(int j = 0; j < 7; j = j + 2){
				//int lastIndex = _arrayOfMoveTiles.size()-1;
				MoveableTile mT = _arrayOfMoveTiles.get(0);
				_board[i][j] = mT;
				_arrayOfMoveTiles.remove(mT);
			}
		}

		MoveableTile mT = _arrayOfMoveTiles.get(_arrayOfMoveTiles.size()-1);
		mT.setLastTileNum(-1);
	}
	
	/**
	 * The method is a getter for the arraylist of movable tiles
	 * @return _arrayOfMoveTiles, the arraylist of moveable tiles
	 * @author Ian, Ken
	 */
	public ArrayList<MoveableTile> getMoveableTileArray(){
		return _arrayOfMoveTiles;
	}
	
	/**this method populates the board w/ between 1 and 4 players in the color order tan, blue,red and white at the 
	 * corresponding correct starting board locations
	 * 
	 * @author Ken, Ian
	*/
	public void createAndPlacePlayers(){
		//
		
		AbstractTile[] at = {_board[2][2], _board[2][4], _board[4][2], _board[4][4]};
		for(int i = 0; i<_numOfPlayers;i++){
			Player p = new Player(Player._validColors[i]);
			p.setCurrentTile(at[i]);
			p.setGameBoard(this);
			_players[i] = p;
			at[i].addPlayer(p);
		}
	}
	/**
	 * The method is a getter that returns a coordinate of the tile
	 * @param row represents the row of the tile
	 * @param col represents the column of the tile
	 * @return the coordinates from the tile 
	 * 
	 * @author Ian, Josh 
	 */
	public AbstractTile getTileFromCoordinates(int row, int col){
		if(row < 0 || row > 48 || col < 0 || col > 48){
			return null;
		}
		return _board[row][col];
	}
	
	/**
	 * this method takes in an AbstractTile parameter aT and checks to see if
	 * the _board contains that tile.  If so, the method returns the coordinates
	 * (row and column) of the tile in the _board array as a return type of
	 * int[] of size 2, where the first index is row and the 2nd is column.
	 * @param aT the reference to the abstract tile whose coordinates we want
	 * @return coord, the int[] coordinates of the tile
	 * 
	 * @author Ian, Josh, Weijin
	 */
	public int[] getCoordinates(AbstractTile aT){
		//check if aT is in _board
		for(int i = 0; i < 7; i++){
			for(int j = 0; j < 7; j++){
				if(aT == _board[i][j]){
					int[] coord = new int[2];
					coord[0] = i;  //i is the row
					coord[1] = j;  //j is the col
					return coord;
				}
			}
		}
		return (new int[] {-1,-1});
		
	}
	
	/**
	 * This method returns the tileNum in range [0,48] if the tile exists
	 * in _board array.  If it is not in the _board then return -1.
	 * @param aT is the reference to abstract tile whose tilenum we would like
	 * @return the tile number variable aT references
	 * 
	 * @author Ian, Weijin
	 */
	public int getTileNumFromTileReference(AbstractTile aT){
		if(aT==null){return -2;}
		int tileNum;
		for(int i = 0; i < 7; i++){
			for(int j = 0; j < 7; j++){
				if(aT == _board[i][j]){
					tileNum = i * 7 + j;
					return tileNum;
				}
			}
		}
		//return -1 if tile not in _board;
		return -1;
	}
	
	/**
	 * This method takes tile number (0-48, where 0th tile is upper left of gameboard
	 * at coordinates (0,0), 1 is (0,1), 47 is (6,5), 48 is (6,6) and so on, and returns
	 * reference to tile object in _board.  Returns a tile if tilNum in range [0,48] and
	 * null otherwise
	 * @param tileNum the tile number of the abstract tile
	 * @return aT the reference to the abstract tile
	 * @author Weijin, Ian 03-18-16
	 */
	public AbstractTile getTileFromTileNumber(int tileNum){
		int tileRow = tileNum / 7;
		int tileCol = tileNum % 7;
		if(0 <= tileNum && tileNum <= 48){
			AbstractTile aT = getTileFromCoordinates(tileRow,tileCol);
			return aT;
		}
		return null;
	}

	/**
	 * This method determines if inserting the shiftable tile (the only remaining MoveableTile
	 * in the _arrayOfMoveTiles after populating the board with tiles) is legal for a given, chosen
	 * insertion tile position.   
	 * 
	 * @param insertionPositionTileNumberOnGameBoard the tile Number of the tile on the board at which insertion begins (not the tile being inserted)
	 * @return boolean true if it is a legal insertion and false otherwise
	 * @author Josh, Weijin 3-20-16
	 * @author Josh, Ian 03-21-16 3-7pm
	 */
	public boolean checkIfInsertShiftableTileLegal(int insertionPositionTileNumberOnGameBoard){
	//	if(_arrayOfMoveTiles.size() != 1){return false;}
		MoveableTile shiftable = _arrayOfMoveTiles.get(0);
		//if n equals a shiftable tileNum location
		if(insertionPositionTileNumberOnGameBoard == 1||insertionPositionTileNumberOnGameBoard == 3
				||insertionPositionTileNumberOnGameBoard == 5||insertionPositionTileNumberOnGameBoard == 7
				||insertionPositionTileNumberOnGameBoard == 13||insertionPositionTileNumberOnGameBoard == 21
				||insertionPositionTileNumberOnGameBoard == 27||insertionPositionTileNumberOnGameBoard == 35
				||insertionPositionTileNumberOnGameBoard == 41||insertionPositionTileNumberOnGameBoard == 43
				||insertionPositionTileNumberOnGameBoard == 45||insertionPositionTileNumberOnGameBoard == 47){
			//return false if during the last turn the shiftable tile existed at tileNum location n
			if(shiftable.getLastTileNum() == insertionPositionTileNumberOnGameBoard){ 
				if(_observer!=null){
					String s = "\t\t\t\tGAME INFO\n\nIt is now " + CURRENTPLAYER.getName() +
							"'s (" + CURRENTPLAYER.getColor() + " pawn) turn."+
							"\nCurrent Collectible Token Number: " + getCurrentTargetTokenValue()+"\n\nIllegel insertion point!"+
							" Try again!";
					_observer.updateGameFeedBack(s);
				}
				return false;
			}
			//else we are free to insert
			else{
				AbstractTile insPointTile = getTileFromTileNumber(insertionPositionTileNumberOnGameBoard);
				int[] coordInsPointTile = getCoordinates(insPointTile);
				int row = coordInsPointTile[0];
				int col = coordInsPointTile[1];
				
				//insert shiftable MoveableTile from left (shift right) of board at one of tileNum 7, 21, 35
				if(insertionPositionTileNumberOnGameBoard == 7 || insertionPositionTileNumberOnGameBoard == 21 
						|| insertionPositionTileNumberOnGameBoard == 35){
					MoveableTile temp = _arrayOfMoveTiles.remove(0);
					int tileNumNewShiftable = insertionPositionTileNumberOnGameBoard + 6;
					MoveableTile newShiftable = (MoveableTile) getTileFromTileNumber(tileNumNewShiftable);
					newShiftable.setLastTileNum(tileNumNewShiftable);
					_arrayOfMoveTiles.add(newShiftable);
					for(int i = 5; i >= 0; i--){
						AbstractTile aT = _board[row][i];
						_board[row][i+1] = aT;
					}
					_board[row][col] = temp;

					if(newShiftable.hasPlayer()){
						ArrayList<Player> players = newShiftable.getPlayers();
						for(Player p: players){
							_board[row][col].addPlayer(p);
							p.setCurrentTile(_board[row][col]);
						}
						newShiftable.getPlayers().clear();
					}
					
					if(newShiftable.hasToken()){
						Token t = newShiftable.removeToken();
						_board[row][col].setToken(t);
						t.setTile(_board[row][col]);
					}
					
					if(_observer!=null){
						_observer.update();	
					}
					if(_observer!=null){
					String s = "\t\t\t\tGAME INFO\n\nIt is now " + GameBoard.CURRENTPLAYER.getName() +
							"'s (" + GameBoard.CURRENTPLAYER.getColor() + " pawn) turn."+
							"\nCurrent Collectible Token Number: " + getCurrentTargetTokenValue();
					_observer.updateGameFeedBack(s);
					}
					return true;
				}
				
				//insert shiftable MoveableTile from right (shift left) of board at one of tileNum 7, 21, 35
				else if(insertionPositionTileNumberOnGameBoard == 13 || insertionPositionTileNumberOnGameBoard == 27 
						|| insertionPositionTileNumberOnGameBoard == 41){
					
					MoveableTile temp = _arrayOfMoveTiles.remove(0);
					int tileNumNewShiftable = insertionPositionTileNumberOnGameBoard - 6;
					MoveableTile newShiftable = (MoveableTile) getTileFromTileNumber(tileNumNewShiftable);

					newShiftable.setLastTileNum(tileNumNewShiftable);
					_arrayOfMoveTiles.add(newShiftable);
					for(int i = 1; i <= 6; i++){
						AbstractTile aT = _board[row][i];
						_board[row][i-1] = aT;
					}
					_board[row][col] = temp;

					if(newShiftable.hasPlayer()){
						ArrayList<Player> players = newShiftable.getPlayers();
						for(Player p: players){
							_board[row][col].addPlayer(p);
							p.setCurrentTile(_board[row][col]);
						}
						players.clear();
					}
					
					if(newShiftable.hasToken()){
						Token t = newShiftable.removeToken();
						_board[row][col].setToken(t);
						t.setTile(_board[row][col]);
					}
					
					if(_observer!=null){
						_observer.update();	
					}
					if(_observer!=null){
						String s = "\t\t\t\tGAME INFO\n\nIt is now " + GameBoard.CURRENTPLAYER.getName() +
								"'s (" + GameBoard.CURRENTPLAYER.getColor() + " pawn) turn."+
								"\nCurrent Collectible Token Number: " + getCurrentTargetTokenValue();
						_observer.updateGameFeedBack(s);
					}
					return true;
				}
				
				//insert shiftable MoveableTile from top (shift down) of board at one of tileNum 7, 21, 35
				if(insertionPositionTileNumberOnGameBoard == 1 || insertionPositionTileNumberOnGameBoard == 3 
						|| insertionPositionTileNumberOnGameBoard == 5){
					
					MoveableTile temp = _arrayOfMoveTiles.remove(0);
					int tileNumNewShiftable = insertionPositionTileNumberOnGameBoard + 42;
					MoveableTile newShiftable = (MoveableTile) getTileFromTileNumber(tileNumNewShiftable);

					newShiftable.setLastTileNum(tileNumNewShiftable);
					_arrayOfMoveTiles.add(newShiftable);
					for(int i = 5; i >= 0; i--){
						AbstractTile aT = _board[i][col];
						_board[i+1][col] = aT;
					}
					_board[row][col] = temp;

					if(newShiftable.hasPlayer()){
						ArrayList<Player> players = newShiftable.getPlayers();
						for(Player p: players){
							_board[row][col].addPlayer(p);
							p.setCurrentTile(_board[row][col]);
						}
						players.clear();
					}
					
					if(newShiftable.hasToken()){
						Token t = newShiftable.removeToken();
						_board[row][col].setToken(t);
						t.setTile(_board[row][col]);
					}
					
					if(_observer!=null){
						_observer.update();	
					}
					if(_observer!=null){
					String s = "\t\t\t\tGAME INFO\n\nIt is now " + GameBoard.CURRENTPLAYER.getName() +
							"'s (" + GameBoard.CURRENTPLAYER.getColor() + " pawn) turn."+
							"\nCurrent Collectible Token Number: " + getCurrentTargetTokenValue();
					_observer.updateGameFeedBack(s);
					}
					return true;
				}
				
				//insert shiftable MoveableTile from bottom (shift up) of board at one of tileNum 7, 21, 35
				else{ // insertionPositionTileNumberOnGameBoard == 43 || insertionPositionTileNumberOnGameBoard == 45 
					  // || insertionPositionTileNumberOnGameBoard == 47
					
					MoveableTile temp = _arrayOfMoveTiles.remove(0);
					int tileNumNewShiftable = insertionPositionTileNumberOnGameBoard - 42;
					MoveableTile newShiftable = (MoveableTile) getTileFromTileNumber(tileNumNewShiftable);

					newShiftable.setLastTileNum(tileNumNewShiftable);
					_arrayOfMoveTiles.add(newShiftable);
					for(int i = 1; i <= 6; i++){
						AbstractTile aT = _board[i][col];
						_board[i-1][col] = aT;
					}
					_board[row][col] = temp;

					if(newShiftable.hasPlayer()){
						ArrayList<Player> players = newShiftable.getPlayers();
						for(Player p: players){
							_board[row][col].addPlayer(p);
							p.setCurrentTile(_board[row][col]);
						}
						players.clear();
					}
					
					if(newShiftable.hasToken()){
						Token t = newShiftable.removeToken();
						_board[row][col].setToken(t);
						t.setTile(_board[row][col]);
					}
					
					if(_observer!=null){
						_observer.update();	
					}
					if(_observer!=null){
					String s = "\t\t\t\tGAME INFO\n\nIt is now " + GameBoard.CURRENTPLAYER.getName() +
							"'s (" + GameBoard.CURRENTPLAYER.getColor() + " pawn) turn."+
							"\nCurrent Collectible Token Number: " + getCurrentTargetTokenValue();
					_observer.updateGameFeedBack(s);
					}
					return true;
				}

					
			}
		}
		//insertionPositionTileNumberOnGameBoard is not a legal insertion position; thus return false
		if(_observer!=null){
		String s = "\t\t\t\tGAME INFO\n\nIt is now " + CURRENTPLAYER.getName() +
				"'s (" + CURRENTPLAYER.getColor() + " pawn) turn."+
				"\nCurrent Collectible Token Number: " + getCurrentTargetTokenValue()+"\n\nIllegel insertion point!"+
				" Try again!";
		_observer.updateGameFeedBack(s);
		}
		return false;
		
	}
	
	/**
	 * This method is updating the game feedback 
	 * @param s the text on game feedback panel
	 * @author Ian, Satya
	 */
	public void updateGameFeedBack(String s){
		if(_observer!=null){
		_observer.updateGameFeedBack(s);
		}
	}
	
	
	 /**
	  * This method checks if a move from a given origin tile to a desination tile is 
	  * legal.  The player calls this method to check if a given move is legal for the
	  * player.  The algorithm follows a path of 1s on a tile's face (the _top, _bottom,
	  * _left, and _right instance variables from AbstractTile class) corresponding to the
	  * path connections between tiles in the game board Master Labyrinth.  A connection
	  * exists between any two adjacent, touching tiles if they share an edge at which
	  * both tiles have a value of 1.  A path exists from an origin tile to a destination
	  * tile is a path of such connecting 1-pairs can be traversed from the origin to 
	  * the destination.
	  *
	  * @param originTile the origin tile of the player 
	  * @param destinationTile the destination tile of the player 
	  * @return true if the path is legal or false if its not a legal move
	  * @author Weijin, Ken, Ian(3-17-16)
	  * @author satya,Ken (03-19-16, 4pm)
	  * @author Ian, Satya 04-10-16
	  */
	public boolean checkIfMoveLegal(AbstractTile originTile, AbstractTile destinationTile){
		//int originTileNum = getTileNumFromTileReference(originTile);
		//int destinationTileNum = getTileNumFromTileReference(destinationTile);
		ArrayList<AbstractTile> tobechecked  = new ArrayList<AbstractTile>();
		tobechecked.add(originTile);
		ArrayList<AbstractTile> possibleDestinations = new ArrayList<AbstractTile>();
		//possibleDestinations.add(originTile);
		while(tobechecked.size()>0){
			AbstractTile currentTile = tobechecked.get(0);
			tobechecked.remove(currentTile);
			int[] currentCoord = getCoordinates(currentTile);
			int currentRow = currentCoord[0];
			int currentCol = currentCoord[1];
			
			//checking tile above current tile
			AbstractTile tileAbove;
			if(currentRow!=0){
				tileAbove = _board[currentRow-1][currentCol];
			} else { tileAbove = null; }
			if(tileAbove != null){
				if(currentTile.getTop()==1 && tileAbove.getBottom() ==1){
					if(!possibleDestinations.contains(tileAbove)){
						tobechecked.add(tileAbove);
						possibleDestinations.add(tileAbove);
					}
				}
			}
			
			//checking tile below current tile
			AbstractTile tileBelow;
			if(currentRow!=6){
				tileBelow = _board[currentRow+1][currentCol];
			} else { tileBelow = null; }
			if(tileBelow != null){
				if(currentTile.getBottom()==1 && tileBelow.getTop() ==1){
					if(!possibleDestinations.contains(tileBelow)){
						tobechecked.add(tileBelow);
						possibleDestinations.add(tileBelow);
					}
				}
			}
			
			//checking tile to left of current tile
			AbstractTile tileLeft;
			if(currentCol!=0){
				tileLeft = _board[currentRow][currentCol-1];
			} else { tileLeft = null; }
			if(tileLeft != null){
				if(currentTile.getLeft()==1 && tileLeft.getRight() ==1){
					if(!possibleDestinations.contains(tileLeft)){
						tobechecked.add(tileLeft);
						possibleDestinations.add(tileLeft);
					}
				}
			}
			
			//checking tile to right of current tile
			AbstractTile tileRight;
			if(currentCol!=6){
				tileRight = _board[currentRow][currentCol+1];
			} else { tileRight = null; }
			if(tileRight != null){
				if(currentTile.getRight()==1 && tileRight.getLeft() ==1){
					if(!possibleDestinations.contains(tileRight)){
						tobechecked.add(tileRight);
						possibleDestinations.add(tileRight);
					}
				}
			}
		
		}
//		System.out.println("The origin tile number is: "+originTileNum+"\nThe destination tile number is: "+ 
//							destinationTileNum);
//		System.out.println("\nThe tile numbers of the destination tiles are: ");
//		for(AbstractTile aT: possibleDestinations){
//			System.out.println(getTileNumFromTileReference(aT));
//		}
//		System.out.print("\n");
		if(possibleDestinations.contains(destinationTile)){
//			System.out.println("Valid Move: There exists a path between origin and destination!");
		}
		else{
//			System.out.println("Invalid Move: There exists NO path between origin and destination.");
		}
		possibleDestinations.remove(originTile);
		return(possibleDestinations.contains(destinationTile));

	} //end of legalMove method
	
	/**
	 * The method is a getter which returns _players
	 * @return _players the array of players in the game
	 * @author Ian, Ken
	 */
	public Player[] getPlayers(){
		return _players;
	}
	
	/**
	 * This method sets the players using parameter 
	 * passed into the method 
	 * @param players array
	 * @author weijin,Satya 04-03-16 5:28pm
	 */
//	public void setPlayers(Player[] players){
//		_players = players;
//	}

//	OUR STAGE 1 CODE DOES NOT NEED THIS PRINTBOARD METHOD -- IT WAS USEFUL IN DEBUGGING,
//	AND MAY BE USEFUL LATER, SO WE LEAVE IT IN.
	/**
	 * The method just creates a version of the board that prints to the console --
	 *  the board that can be used to check if everything is working correctly
	 *  it uses the 1 or 0 in each direction to show if the path is there or not
	 *  example:     0
	 * 			  0		1     this is a L shape tile that allows a path on SE and S
	 * 				 1	
	 * @author Ian,Ken
	 */
	public void printBoard(){
		
		System.out.println("     Col: 0\t\t\t     Col: 1\t\t\t     Col: 2\t\t\t     Col: 3\t\t\t     Col: 4\t\t\t     Col: 5\t\t\t     Col: 6");
		
		for(int i = 0; i < 7; i++){
			System.out.println("Row: " + i);
			for(int j = 0; j < 7; j++){
				System.out.print("\t" + _board[i][j]._top + " \t\t\t");
			}	
			System.out.print("\n\n");
			for(int j = 0; j < 7; j++){
				if(_board[i][j].hasPlayer()){
					String allColors = "";
					for(Player p: _board[i][j].getPlayers()){
						allColors = allColors + " " + p.getColor().charAt(0); 
					}
					System.out.print(_board[i][j]._left + "\t"+ allColors +"\t" + _board[i][j]._right + "\t\t");
				}
				else{
					System.out.print(_board[i][j]._left + "\t\t" + _board[i][j]._right + "\t\t");
				}
			}	
			System.out.print("\n\n");
			for(int j = 0; j < 7; j++){
				System.out.print("\t" + _board[i][j]._bottom + "\t\t\t");
			}
			System.out.print("\n\n");
		}
		
		System.out.print("\n\nShiftable Tile: \n");
		MoveableTile shiftable = _arrayOfMoveTiles.get(0);
		System.out.print("\t" + shiftable.getTop() + "\n\n" + shiftable.getLeft() + "\t\t" + shiftable.getRight() +
						"\t\t\n\n\t" + shiftable.getBottom() + "\n"); 
	}
	
	/**
	 * This methods sets up the observer
	 * @return observer the state of the game
	 * @author Ian,Weijin 
	 */
	public GameBoardGUI getObserver() {
		return _observer;
	}
	
	/**
	 * This method assigns the observer
	 * @param observer the state of the game
	 * @author Ian,Weijin
	 */
	public void setObserver(GameBoardGUI observer) {
		
		_observer = observer;
	}
	
	/**
	 *This method updates the observer
	 * @author Ian,Weijin
	 */
	public void playerHasAlteredBoard(){	
		if(_observer!=null){
			_observer.update();	
		}
	
	}
	/**
	 * This method assigns the turn
	 * @param turns whose turn is it
	 * @author ken,Josh (April 3,2016)(8:20pm)
	 */
//	public void setTurn(int turns){
//		_turns = turns;
//	}
	/**
	 * This method determines whose turn is it
	 * @param turns whose turn it is 
	 * @author ken,Josh (April 3,2016)(8:20pm)
	 */
//	public int getCurrentTurn(){
//		return _turns;
//	}
	
	/**
	 * This method keeps each player's order of his/her turn
	 * @param turns
	 * @author ken,Josh (April 3,2016)(8:20pm)
	 */
	//This is just a basic and maybe stupid structure for counting the player turns
	//large code needs to be updated
//	public void Turns(String s){		
//		
//		GameBoard gb = new GameBoard(4);
//		int _p = gb._numOfPlayers;
//		int state = 0;
//		
//		if (_p == 2){
//			switch(state){
//			case 0:
//			}
//		}
//		
//		if (_p == 3){
//			switch(state){}
//		}
//		
//		if (_p == 4){
//			switch(state){}
//		}
//	}
	
	/**
	 * This method changes currentPlayer to the next currentPlayer (i.e., if player 1 ends turn,
	 * now it is player 2's turn)
	 * @author Ian, Satya 04-10-16
	 */
	public void toggleNextPlayer(){
		currentPlayerIndex = (currentPlayerIndex + 1) % _players.length;
		CURRENTPLAYER = _players[currentPlayerIndex];
	}
	
	/**
	 * This method toggles currentTargetToken to the next token
	 * @author weijin,Satya
	 */
	public void toggleNextToken(){
		if(_currentTargetToken.getValue() == 25){ 
			GAMEOVER = true;
			
			_observer.gameOver();
			
		}
		else{
			int index = _tokens.indexOf(_currentTargetToken);
			_currentTargetToken = _tokens.get(index+1);
			System.out.println("The old taget token number was: " + _tokens.get(index).getValue());
			System.out.println("The new target token is token number: " + _currentTargetToken.getValue());
		}
	}
	/**
	 * This method returns a string representation of the GameBoard formatted for the save file
	 * @return the string representation of the GameBoard
	 */
	public String toString(){
		String s = "";
		for (int i = 0; i<_players.length; i++){
			s = s + _players[i].toString();
			if (i != _players.length-1){
				s = s + ",";
			}
		}
		s = s + " \n";
		for (int i = 0; i<7; i++){
			for (int k = 0; k<7; k++){
				s = s + _board[i][k].toString();
				if (i != 6){
					s = s + ",";
				}
				if (i ==6 && k != 6) s = s+",";
			}
		}
		s = s + " \n";
		MoveableTile t = _arrayOfMoveTiles.get(0);
		switch(t.getLastTileNum()){
		case 1: s = s + "1"; break;
		case 3: s = s + "2"; break;
		case 5: s = s + "3"; break;
		case 13: s = s + "4"; break;
		case 27: s = s + "5"; break;
		case 41: s = s + "6"; break;
		case 47: s = s + "7"; break;
		case 45: s = s + "8"; break;
		case 43: s = s + "9"; break;
		case 35: s = s + "10"; break;
		case 21: s = s + "11"; break;
		case 7: s = s + "12"; break;
		default: s = s + "0"; break;
		}
		return s;
	}
	
	/**
	 * This method creates all possible formula cards and adds them to an ArrayList of formula cards,
	 * then shuffles the deck
	 */
	public void arrayOfFormula(){
		_listOfCards = new ArrayList<FormulaCards>();
		
		for(int i =0; i<21;i++){
			FormulaCards temp;
			switch(i){
			case 0: temp = new FormulaCards();
					temp.AddNames("Magic Wand Made of Blindworm", "Oak leaf", "Fire Salamanders");
					temp.AddValue(11, 3, 14);
					_listOfCards.add(temp);
					break;
			case 1: temp = new FormulaCards();
					temp.AddNames("Four-leaf clover", "Berries of Mistletoe", "Emerald");
					temp.AddValue(5, 25, 18);
					_listOfCards.add(temp);
					break;	
			case 2: temp = new FormulaCards();
				temp.AddNames("Root of Mandrake", "Raven's feather", "Weasel Spit");
				temp.AddValue(19, 7, 15);
				_listOfCards.add(temp);
				break;
			case 3: temp = new FormulaCards();
				temp.AddNames("Crab apple", "Skull moss", "Toad");
				temp.AddValue(1, 10, 13);
				_listOfCards.add(temp);
				break;
			case 4: temp = new FormulaCards();
				temp.AddNames("Oil of Black Slugs", "Snake", "Oak leaf");
				temp.AddValue(20, 17, 3);
				_listOfCards.add(temp);
				break;
			case 5: temp = new FormulaCards();
				temp.AddNames("Spider", "Black Rooster", "Magic Wand Made of Blindworm");
				temp.AddValue(9, 20, 11);
				_listOfCards.add(temp);
				break;
			case 6: temp = new FormulaCards();
				temp.AddNames("Garlic", "Fire Salamander", "HenBane");
				temp.AddValue(6, 14, 8);
				_listOfCards.add(temp);
				break;
			case 7: temp = new FormulaCards();
				temp.AddNames("Fire Salamander", "Oil of Black Slug", "Skull moss");
				temp.AddValue(14, 4, 10);
				_listOfCards.add(temp);
				break;
			case 8: temp = new FormulaCards();
				temp.AddNames("Toad", "Weasel Spit", "Quartz");
				temp.AddValue(13, 15, 12);
				_listOfCards.add(temp);
				break;
			case 9: temp = new FormulaCards();
				temp.AddNames("Weasel Spit", "Pine Cones", "Oil of Black slug");
				temp.AddValue(15, 2, 4);
				_listOfCards.add(temp);
				break;
			case 10: temp = new FormulaCards();
				temp.AddNames("Oil of Black slug", "Toad", "Black Rooster");
				temp.AddValue(4, 13, 20);
				_listOfCards.add(temp);
				break;
			case 11: temp = new FormulaCards();
				temp.AddNames("Skull moss", "Quartz", "Silver Thistle");
				temp.AddValue(10, 12, 16);
				_listOfCards.add(temp);
				break;
			case 12: temp = new FormulaCards();
				temp.AddNames("Silver Thistle", "Spider", "Raven's feather");
				temp.AddValue(16, 9, 7);
				_listOfCards.add(temp);
				break;
			case 13: temp = new FormulaCards();
				temp.AddNames("Pine cone", "Henbane", "Snake");
				temp.AddValue(2, 8, 17);
				_listOfCards.add(temp);
				break;
			case 14: temp = new FormulaCards();
				temp.AddNames("Snake", "Four-leaf clover", "Garlic");
				temp.AddValue(17, 5, 6);
				_listOfCards.add(temp);
				break;
			case 15: temp = new FormulaCards();
				temp.AddNames("HenBane", "Root of Mandrake", "Four-leaf clover");
				temp.AddValue(8, 19, 5);
				_listOfCards.add(temp);
				break;
			case 16: temp = new FormulaCards();
				temp.AddNames("Four-leaf clover", "Emerald", "Crab Apple");
				temp.AddValue(5, 18, 1);
				break;
			case 17: temp = new FormulaCards();
				temp.AddNames("Quartz", "Crab Apple", "Spider");
				temp.AddValue(12, 1, 9);
				_listOfCards.add(temp);
				break;
			case 18: temp = new FormulaCards();
				temp.AddNames("Emerald", "Magic Wand Made of Blindworm", "Root of Mandrake");
				temp.AddValue(18, 11, 19);
				_listOfCards.add(temp);
				break;
			case 19: temp = new FormulaCards();
				temp.AddNames("Berries of Mistletoe", "Silver Thistle", "Pine Cone");
				temp.AddValue(25, 16, 2);
				_listOfCards.add(temp);
				break;
			case 20: temp = new FormulaCards();
				temp.AddNames("Raven's Feather", "Garlic", "Berries of Mistletoe");
				temp.AddValue(7, 6, 25);
				_listOfCards.add(temp);
				break;
			}// end of switch 
		}	
		Collections.shuffle(_listOfCards);
		System.out.println(_listOfCards.get(0).getNames()[0]);
	}

	
	
} //end of Game Board class definition
