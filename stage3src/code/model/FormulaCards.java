package code.model;

public class FormulaCards {
	
	private int _length;
	private String[] _names;
	private int[] _value;
	/**
	  This method creates formula cards for each player
	  Each card has an array size of 3, and name of ingredients as string
	  **/
	public FormulaCards(){
		_length=3;
		_names = new String[_length];
		_value = new int[_length];
	}// end of FormulaCArds
	/**
	 Adds 3 different names to the different indexes of the card
	  **/
	public void AddNames(String a, String b, String c){
		_names[0] = a;
		_names[1] = b;
		_names[2] = c;
	}// end of AddNAme
	/**
	 Adds various token values to the matching array index (if they match) 
	 */
	public void AddValue(int a, int b, int c){
		_value[0] = a;
		_value[1] = b;
		_value[2] = c;
	}// end of AddValue
	/**
		Returns true/checks if the formula card indexes matches the value
	 */
	public boolean Ingrediant(int a){
		boolean temp = false;
		
		for(int i =0; i<_length; i++){
			if(_value[i]== a)
			temp = true;
		}
		
		return temp;
	}
	/**
	 Gets value of items in the formula card array
	 */
	public int[] getValues(){
		
		return _value;
	}
	/**
	  Gets names of ingredients on the formula card
	 */
	public String[] getNames(){
		return _names;
	}
	
	/**
	 * This method returns a string representation of the formula card formatted for the save file
	 * @return the string representation of the formula card
	 */
	public String toString(){
		String s = "";
		s = s+_value[0] + ",";
		s = s+_value[1] + ",";
		s = s+_value[2] + ",";
		return s;
	}

}// end of class
